import React from 'react';
import Select from 'react-select';
import makeSelectAsync from './MyAsyncSelect.js';

const Comp = makeSelectAsync(Select);

const { REACT_APP_API_URL: apiUrl } = process.env;

function SingleValue(components) {
  return function(props) {
    const data = props.data;

    const label = !data.unit || data.unit.toLowerCase() === 'c/u'
      ? data.articulo
      : data.unit + ' ' + data.articulo;

    return (
      <components.SingleValue {...props}>
        {label}
      </components.SingleValue>
    );
  }
}

function styles(props) {
  return {
    option: (styles, { isDisabled, isFocused, isSelected, value }) => {
      const isElim = value === props.ELIM;

      return {
        ...styles,
        padding: '0px',
        backgroundColor: isDisabled
          ? null
          : isSelected
            ? styles.backgroundColor
            : isFocused
              ? isElim ? 'red' : 'yellow'
              : styles.backgroundColor,
        fontWeight: isElim ? 'bold' : styles.fontweight,
      };
    },

    singleValue: (base) => {
      // fontSize inherited from valueContainer
      return {
        ...base,
        fontSize: 'inherit',
        padding: '5px 0px'
      };
    },

    valueContainer: (base) => {
      //base = {...base, fontSize: '1.5em'};
      return {
        ...base,
        //fontSize: '1.5em',
        //fontSize: 'inherit',
        padding: '0px 0px'
      };
    },
  };
}

function formatOptionLabel(option, FormatOptionLabelMeta) {
  const genStyle = {border: '1px solid black'};
  return ( 
    <table style={{borderCollapse: 'collapse', width: '100%'}}>
      <tbody>
        <tr style={genStyle}>
          <td style={{...genStyle, width: '15%'}}>{option.codProveedor}</td>
          <td style={{...genStyle, padding: '10px 4px 10px 4px'}}>{option.articulo}</td>
          <td style={{...genStyle, width: '15%'}}>{option.marcaPais}</td>
          <td style={{...genStyle, width: '10%'}}>{option.unit}</td>
          <td style={{...genStyle, width: '10%', textAlign: 'right'}}>{option.price}</td>
        </tr>
      </tbody>
    </table>
  );
}

export default class ProductSelect extends React.Component
{
  mapProductsForSelect(data) {
    const content = data.content
      .map((item, index) => ({
        value: item.CODIGO,  // Server must ensure uniqueness of CODIGO
        label: item.ARTICULO,
        codProveedor: item.COD_PROVEEDOR,
        articulo: item.ARTICULO,
        marcaPais: item['MARCA PAIS'],
        unit: item.UNID,
        price: item['P.V.P.'],
        sinIva: item.SIN_IVA
      }))
      .concat([{
        value: this.props.ELIM,
        label: 'ELIMINAR...',
        codProveedor: '',
        articulo: 'ELIMINAR...',
        marcaPais: '',
        unit: '',
        price: '',
        sinIva: true
      }]);
    return {...data, content };
  }

  requestProducts = (input, pageNumber, action) => {
    const param = action && action.action === 'set-unique' ? 'code' : 'any';
    let url = `${apiUrl}/search-product?${param}=`;
    if (input)
      url = url + input
    //console.log('url any', url)
    url = url + `&pageNumber=${pageNumber}`;
    return fetch(url)
      .then(response => response.json())
      .then(data =>
        data && data.content
          ? this.mapProductsForSelect(data)
          : Promise.reject(null)
      );
  }

  setInput(input) {
    this.ref.onInputChange(input, { action: 'set-unique' }, ([option]) => {
      this.props.onProductSelected(option);
      this.ref.onInputChange('', { action: 'set-value' });
    });
  }

  render() {
    const v = SingleValue(this.props.components);
    return (
      <Comp
        {...this.props}
        ref={e => this.ref = e}
        styles={{...this.props.styles, ...styles(this.props)}}
        request={this.requestProducts}
        formatOptionLabel={formatOptionLabel}
        onOptionSelected={this.props.onProductSelected}
        components={{...this.props.components, SingleValue: v}}
      />
    );
  }
}
