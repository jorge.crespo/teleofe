import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import purple from '@material-ui/core/colors/purple';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: 'none',
  },
  cssLabel: {
    '&$cssFocused': {
      color: purple[500],
    },
  },
  cssFocused: {},
  cssUnderline: {
    '&:after': {
      borderBottomColor: purple[500],
    },
  },
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: purple[500],
    },
  },
  notchedOutline: {},
});

function CustomizedInputs(props) {
  const { classes } = props;
  const inputProps = props.inputProps || {};

  return (
    <div className={classes.root}>
      <FormControl
        className={classes.margin}
        style={{width: '100%'}}
        disabled={props.disabled}
      >
        <InputLabel
          htmlFor="custom-css-standard-input"
          classes={{
            root: classes.cssLabel,
            focused: classes.cssFocused,
          }}
          //style={{fontSize: 'inherit'}}
        >
          {props.label}
        </InputLabel>
        <Input
          id="custom-css-standard-input"
          classes={{
            underline: classes.cssUnderline,
          }}
          inputProps={{
            ...inputProps,
            spellCheck: false,
            style: {
              //fontSize: '1.5em',
              padding: '6px 5px 5px 5px'
            },
          }}
          style={{...props.InputStyle, fontFamily: 'inherit'}}
          type={props.type || 'text'}
          value={props.value}
          onChange={props.onChange}
          inputRef={props.inputRef}
        />
      </FormControl>
    </div>
  );
}

CustomizedInputs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CustomizedInputs);
