/* eslint-disable react/prop-types, react/jsx-handler-names */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import NoSsr from '@material-ui/core/NoSsr';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';

import purple from '@material-ui/core/colors/purple';
import { isEmpty, isFunction } from 'lodash';


const styles = theme => ({
  root: {
    //flexGrow: 1,
    //height: 250,
  },
  input: {
    display: 'contents',
    padding: 0,
  },
  cssLabel: {
    '&$cssFocused': {
      color: purple[500],
    },
  },
  cssFocused: {},
  cssUnderline: {
    '&:before': {
      bottom: '3px',
    },
    '&:after': {
      bottom: '3px',
      borderBottomColor: purple[500],
    },
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden',
  },
  noOptionsMessage: {
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
  singleValue: {
    fontSize: 16,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16,
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0,
  },
  divider: {
    height: theme.spacing(2),
  },
});

function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

function Control(props) {
  return (
    <TextField
      fullWidth
      InputProps={{
        disabled: props.isDisabled,
        ...props.selectProps.textFieldInputProps,
        inputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps,
        },
      }}
      {...props.selectProps.textFieldProps}
    />
  );
}

function Option(props) {

  let innerStyle = isFunction(props.getStyles)
    ? props.getStyles('option', props) || {}
    : {};

  return (
    <MenuItem
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 400,
        ...innerStyle,
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

/*
 * In case of Placeholder, please modify...
 *
function Placeholder(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.placeholder}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}
 */

function SingleValue(props) {

  let innerStyle = isFunction(props.getStyles)
    ? props.getStyles('singleValue', props) || {}
    : {};

  return (
    <Typography className={props.selectProps.classes.singleValue} style={innerStyle} {...props.innerProps}>
      {props.children}
    </Typography>
  );
}

function ValueContainer(props) {
  let innerStyle = isFunction(props.getStyles)
    ? props.getStyles('valueContainer', props) || {}
    : {};

  return <div style={innerStyle}>{props.children}</div>;
}

function Menu(props) {
  return (
    <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
      {props.children}
    </Paper>
  );
}

const components = {
  Control,
  //Menu,
  NoOptionsMessage,
  //Option,
  //Placeholder,
  SingleValue,
  ValueContainer,
};

class IntegrationReactSelect extends React.Component {
  state = {
    hasInputFocus: false,
  };

  handleGotFocus = (event) => {
    this.setState({ hasInputFocus: true });

    const { onFocus } = this.props;

    if (isFunction(onFocus)) {
      onFocus(event);
    }
  };

  handleLostFocus = (event: any) => {
    this.setState({ hasInputFocus: false });

    const { onBlur } = this.props;

    if (isFunction(onBlur)) {
      onBlur(event);
    }
  }

  hasInputFocus() {
    return this.state.hasInputFocus === true;
  }

  render() {
    const { classes, theme, label, value } = this.props;
    const shrink = this.hasInputFocus() || isEmpty(value.label) === false;

    const selectStyles = {
      input: base => ({
        ...base,
        color: theme.palette.text.primary,
        '& input': {
          font: 'inherit',
        },
      }),
    };

    let theProps = {
      value,
      classes,
      styles: selectStyles,
      textFieldProps: {
        label,
        InputLabelProps: {
          shrink,
          classes: {
            root: classes.cssLabel,
            focused: classes.cssFocused,
          },
          //style: {fontSize: '24px'},
          //style: {fontSize: 'inherit'},
        },
      },
      textFieldInputProps: {
        classes: {
          underline: classes.cssUnderline,
        },
        //style: {height: '40px'},
      },
      components,
      onFocus: this.handleGotFocus,
      onBlur: this.handleLostFocus,
      placeholder: (label ? '' : this.props.placeholder),
    };

    return (
      <div className={classes.root}>
        <NoSsr>
          {this.props.render(theProps)}
        </NoSsr>
      </div>
    );
  }
}

IntegrationReactSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(IntegrationReactSelect);
