import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import FileUploadIcon from '@material-ui/icons/CloudUpload';
import { useFilePicker } from 'use-file-picker';
import React from 'react';

export default function App(props) {
  const [openFileSelector, { plainFiles, loading, clear }] = useFilePicker({
    accept: '.xlsx',
    readFilesContent: false,
  });

  React.useMemo(() => {
    plainFiles.forEach((file, index) => {
      console.log('file chosen, index:', index, 'file:', file);
      props.forChosenFile(file);
    });
    if (plainFiles.length > 0) {
      clear();
    }
  }, [plainFiles]);

/*
  if (loading) {
    return <div>Loading...</div>;
  }
*/

  if (loading) {
    return (
      <div>
        <ListItem button key={'productos'}>
          <ListItemText primary={'Loading...'} />
        </ListItem>
      </div>
    );
  }

  return (
    <div>
      <ListItem button key={'productos'} onClick={openFileSelector}>
        <ListItemIcon><FileUploadIcon /></ListItemIcon>
        <ListItemText primary={'Cargar Productos'} />
      </ListItem>
    </div>
  );
}
