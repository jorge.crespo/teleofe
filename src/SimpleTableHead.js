import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';

import ProformaDialog from './ProformaDialog.js';
import CircularProgress from '@material-ui/core/CircularProgress';
import red from '@material-ui/core/colors/red';
import ConfirmationDialog from './ConfirmationDialog';
import WaitDialog from './WaitDialog';
import InformationDialog from './InformationDialog';

import GenerateProformaPDF from './GenerateProformaPDF';

//import comprobanteTypes from './comprobanteTypes';
//import transactionStates from './transactionStates';

import moment from 'moment';

const { REACT_APP_API_URL: url } = process.env;

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const headRows = [
  { id: 'date', numeric: false, disablePadding: false, label: 'Fecha' },
  { id: 'type', numeric: false, disablePadding: false, label: 'Tipo' },
  //{ id: 'transactionState', numeric: false, disablePadding: false, label: 'Estado de la transacción' },
  { id: 'documentState', numeric: false, disablePadding: false, label: 'Estado' },
  { id: 'number', numeric: true, disablePadding: false, label: 'Número' },
];

function SimpleTableHead(props) {
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox"/>
        {headRows.map(row => (
          <TableCell
            key={row.id}
            align={row.numeric ? 'right' : 'left'}
            padding={row.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === row.id ? order : 'desc'}
          >
            <TableSortLabel
              active={orderBy === row.id}
              direction={orderBy === row.id ? order : 'desc'}
              onClick={createSortHandler(row.id)}
            >
              {row.label}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

SimpleTableHead.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  //checkbox: {},
  //invisible: {
    //visibility: 'hidden',
  //},
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    color: red[500],
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1,
  },
}));

const SimpleTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subtitle1">
            {`${numSelected} seleccionada${plural(numSelected)}`}
          </Typography>
        ) : (
          <Typography variant="h6" id="tableTitle">
            {props.title}
          </Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {numSelected > 0 && (
          <Tooltip title="Delete">
            <div className={classes.wrapper}>
              <IconButton aria-label="Delete" onClick={props.handleDelete}>
                <DeleteIcon />
              </IconButton>
              { props.loading && <CircularProgress size={48} className={classes.buttonProgress} /> }
            </div>
          </Tooltip>
        )}
      </div>
    </Toolbar>
  );
};

SimpleTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  errorCell: {
    color: 'red',
  },
  generatedCell: {
    color: 'blue',
  },
  signedCell: {
    color: 'orange',
  },
  sentCell: {
    color: 'green',
  },
}));

function plural(numElements, c = 's') {
  return numElements !== 1 ? c : '';
}

const g = (a, f) => x => x ? g(a.then(_ => f(x)), f) : a;

const chain = (arr, f) => [...arr.slice(1), undefined].reduce(
  (acum, x) => acum(x),
  g(Promise.resolve(arr[0] !== undefined && f(arr[0])), f)
);

function rowKey(row) {
  return `${row.type}-${row.number}`;
}

/*
function getTransactionState(file) {
  const splitted = file.split('.');
  return splitted.slice(-2, -1)[0];
}
 */

/*
function getComprobanteState(row) {
  if (row.reversed) {
    return 'Anulado';
  }
  if (row.hasError) {
    const ts = getTransactionState(row.file);
    let postfix = 'la verificación';

    if (ts === 'generated') {
      postfix = 'la firma';
    }

    if (ts === 'signed') {
      postfix = 'el envío';
    }

    return `Error en ${postfix}`;
  }
  return '';
}
*/

function getDocumentState(row) {
  return '';
}

export default function SimpleTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState('desc');
  const [orderBy, setOrderBy] = React.useState('number');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [totalCount, setTotalCount] = React.useState(0);
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [drawerLoaded, setDrawerLoaded] = React.useState(false);
  const [pdfToShow, setPdfToShow] = React.useState({});
  const [jsonFilePath, setJsonFilePath] = React.useState(null);
  const [hasError, setHasError] = React.useState(false);
  //const [transactionState, setTransactionState] = React.useState(null);
  const [documentType, setDocumentType] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  const [confirmationDialogOpen, setConfirmationDialogOpen] = React.useState(false);
  const [informationDialogOpen, setInformationDialogOpen] = React.useState(false);
  const [waitDialogOpen, setWaitDialogOpen] = React.useState(false);
  const [successfullyDeleted, setSuccessfullyDeleted] = React.useState([]);
  const [failinglyDeleted, setFailinglyDeleted] = React.useState([]);
  //const [intervalOn, setIntervalOn] = React.useState(false);
  let checkboxClicked = false;

  const handleRequestSort = React.useCallback(function (event, property) {
    const isDesc = orderBy === property && order === 'desc';
    setOrder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  }, [order, orderBy]);

  function handleCheckboxClick(event, row) {
    checkboxClicked = true;

    setSelected(prevSelected => {
      const selectedIndex = prevSelected.findIndex(e => e.type === row.type && e.number === row.number);
      let newSelected = [];

      if (selectedIndex === -1) {
        newSelected = prevSelected.concat(row);
      } else if (selectedIndex === 0) {
        newSelected = prevSelected.slice(1);
      } else if (selectedIndex === prevSelected.length - 1) {
        newSelected = prevSelected.slice(0, -1);
      } else if (selectedIndex > 0) {
        newSelected = prevSelected.slice(0, selectedIndex)
          .concat(prevSelected.slice(selectedIndex + 1));
      }

      return newSelected;
    });
  }

  async function handleViewer(row) {
    if (checkboxClicked)
      return;

    setDrawerOpen(true);
    setDrawerLoaded(false);

    const filePath = row.file;
    const json = await fetch(`${url}/file?file=${filePath}`)
      .then(response => response.json());
    const { doc, uri } = GenerateProformaPDF(json);
    setJsonFilePath(filePath);
    setPdfToShow({ doc, uri });
    setHasError(row.hasError);
    setDocumentType(row.type);
  }

  const openConfirmationDialog = React.useCallback(function () {
    setConfirmationDialogOpen(true);
  }, []);

  function handleConfirmationDialogYes() {
    setConfirmationDialogOpen(false);
    setWaitDialogOpen(true);
    deleteSelectedProformas();
  }

  function handleConfirmationDialogNo() {
    setConfirmationDialogOpen(false);
  }

  function handleConfirmationDialogClose(_, reason) {
    if (reason !== 'backdropClick') {
      setConfirmationDialogOpen(false);
    }
  }

  function deleteSelectedProformas() {
    setLoading(true);
    _deleteSelectedProformas().then(result => {
      setSuccessfullyDeleted(result.successful);
      setFailinglyDeleted(result.failed);
      setLoading(false);
      setSelected([]);
      setWaitDialogOpen(false);
      setInformationDialogOpen(true);
    });
  }

  function _deleteSelectedProfomas() {
    return new Promise((resolve, reject) => {
      const successful = [];
      const failed = [];

      chain(selected, row => deleteProforma(row.number, row.date, row.format)
        .then(data => {
          row.deleted = true;
          successful.push(data);
          return data;
        })
        .catch((error) => {
          failed.push(error);
          return error;
        })
      )
      .then(_ => resolve({ successful, failed }));
    });
  }

  function deleteProforma(sequence, date, dateFormat) {
    return fetch(`${url}/proforma/${sequence}`, {
      method: 'DELETE'
    })
    .then((response) => {
      return response.json()
        .then(data => response.ok ? data : Promise.reject(data));
    });
  };

  function handleInformationDialogAccept() {
    setInformationDialogOpen(false);
    setSuccessfullyReversed([]);
    setFailinglyReversed([]);
  }

  function handleInformationDialogClose(_, reason) {
    if (reason !== 'backdropClick') {
      handleInformationDialogAccept();
    }
  }

  function handleChangePage(event, newPage) {
    setPage(newPage);
  }

  function handleChangeRowsPerPage(event) {
    setRowsPerPage(+event.target.value);
    setPage(0); // We'll show from the beginning again
  }

  function handleChangeDense(event) {
    setDense(event.target.checked);
  }

  function handleChangeIntervalOn(event) {
    setIntervalOn(event.target.checked);
  }

  function buildInformationMessageArray() {
    const dir = 'proformas/';
    const sd = successfullyDeleted
      .filter(e => e.message != 'Starter')
      .map(e => e.message.startsWith(dir)
        ? e.message.substr(dir.length)
        : e.message
      );
    const fd = failinglyDeleted
      .map(e => e.message !== undefined ? e.message : e);

    const firstLine = `${sd.length} proforma${plural(sd.length)} borrada${plural(sd.length)} exitósamente${sd.length > 0 ? '. Notas de crédito generadas:' : '.'}`;
    const secondLine = `${sd.join(', ')}`;
    const thirdLine = `${fd.length || 'Ninguna'} falla${fd.length > 1 ? 's' : ''}${fd.length > 0 ? ':' : '.'}`;
    const fourthLine = `${fd.join(', ')}`;

    return [firstLine, secondLine, thirdLine, fourthLine];
  }

  function request(baseUrl, rowsPerPage, page, order, orderBy) {
    const url = baseUrl + '?' +
      `&rowsPerPage=${rowsPerPage}` +
      `&page=${page}` +
      `&order=${order}` +
      `&orderBy=${orderBy}`;

    return fetch(url)
      .then(response => response.json());
  }

  const updatePage = () => {
    const fetchRows = async () => {
      try {
        const data = await request(props.url, rowsPerPage, page, order, orderBy);
        setRows(data.content);
        setTotalCount(data.totalCount);
      } catch(err) {
        console.log(`Error on requesting to ${props.url}, error:`, err);
        if (rows == null) {
          setRows([]);
          setTotalCount(0);
        }
        return err;
      }
    };
    fetchRows();
  };

  React.useEffect(updatePage, [props.url, rowsPerPage, page,  order, orderBy]);

  React.useEffect(() => { checkboxClicked = false }, [selected]);

/*
  React.useEffect(() => {
    console.log('******** simple starting interval');
    const interval = setInterval(updatePage, 2000);
    return () => { console.log('********* simple clearing interval'); clearInterval(interval); }
  }, [props.url, rowsPerPage, page,  order, orderBy]);
*/

  const getSelectedIndex = row => selected.findIndex(e => e.type === row.type && e.number === row.number);
  const isSelected = row => getSelectedIndex(row) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length);

  const showCheckbox = props.transactionState === 'verified';

  function handleDialogClose() {
    setDrawerOpen(false);
    setHasError(false);
  }

  function onMiniPrint(file) {
    if (props.onMiniPrint) {
      props.onMiniPrint(file);
    }
  }

  function onEdit(file) {
    if (props.onEdit) {
      const jsonFile = file;
      props.onEdit(jsonFile);
    }
  }

  function onFacturar(file) {
    if (props.onFacturar) {
      const noext = file.split('.').slice(0, -1).join('.');
      const jsonFile = `${noext}.json`;
      props.onFacturar(jsonFile);
    }
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <SimpleTableToolbar
          title={props.title}
          numSelected={selected.length}
          handleDelete={openConfirmationDialog}
          loading={loading}
        />
        <div className={classes.tableWrapper}>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
          >
            <SimpleTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {React.useMemo(() => stableSort(rows, getSorting(order, orderBy))
                .map((row, index) => {
                  const isItemSelected = isSelected(row);
                  const labelId = `enhanced-table-checkbox-${index}`;

                        /* Never worked...
                          className={clsx(classes.checkbox, {
                            [classes.invisible]: row.reversed || !showCheckbox,
                          })}
                         */
                          //onClick={handleCheckboxClick2(row)}
                      /*
                      <TableCell
                        align="left"
                        className={clsx({
                          [classes.generatedCell]: getTransactionState(row.file) === 'generated',
                          [classes.signedCell]: getTransactionState(row.file) === 'signed',
                          [classes.sentCell]: getTransactionState(row.file) === 'sent',
                        })}
                      >
                        {transactionStates[getTransactionState(row.file)]}</TableCell>
                       */
                  return (
                    <TableRow
                      hover
                      onClick={() => handleViewer(row)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={rowKey(row)}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        {showCheckbox && !row.reversed && row.type === 'fac' && <Checkbox
                          onClick={event => handleCheckboxClick(event, row)}
                          checked={isItemSelected || !!row.reversed}
                          inputProps={{ 'aria-labelledby': labelId }}
                          disabled={!!row.reversed}
                        />}
                      </TableCell>
                      <TableCell component="th" id={labelId} scope="row" padding="none">
                        {row.date}
                      </TableCell>
                      <TableCell align="left">{row.type}</TableCell>
                      <TableCell
                        align="left"
                        className={clsx({
                          [classes.errorCell]: row.hasError
                        })}
                      >
                        {getDocumentState(row)}
                      </TableCell>
                      <TableCell align="right">{row.number}</TableCell>
                    </TableRow>
                  );
                }), [rows, order, orderBy, selected])}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 49) * emptyRows }}>
                  <TableCell colSpan={headRows.length + 1} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[10, 25, 50]}
          component="div"
          count={totalCount}
          rowsPerPage={rowsPerPage}
          labelRowsPerPage={'Filas por página'}
          labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
      <FormControlLabel
        control={<Switch checked={dense} onChange={handleChangeDense} />}
        label="Relleno denso"
      />
      {/*props.transactionState === 'pending' && <FormControlLabel
        control={<Switch checked={intervalOn} onChange={handleChangeIntervalOn} />}
        label="Refrescar automáticamente"
      />*/}

        {/*transactionState={transactionState}
        onMoveToSent={onMoveToSent}*/}
      <ProformaDialog
        open={drawerOpen}
        setOpen={setDrawerOpen}
        loaded={drawerLoaded}
        setLoaded={setDrawerLoaded}
        file={jsonFilePath}
        uri={pdfToShow.uri}
        doc={pdfToShow.doc}
        hasError={hasError}
        documentType={documentType}
        onClose={handleDialogClose}
        onMiniPrint={onMiniPrint}
        onEdit={onEdit}
        onFacturar={onFacturar}
      />

      <ConfirmationDialog
        title={'Confirme la eliminación por favor'}
        message={`Está seguro que desea borrar la${plural(selected.length)} proforma${plural(selected.length)} seleccionada${plural(selected.length)}?`}
        open={confirmationDialogOpen}
        onYes={handleConfirmationDialogYes}
        onNo={handleConfirmationDialogNo}
        onClose={handleConfirmationDialogClose}
      />

      <WaitDialog
        disableEscapeKeyDown
        title={'Espere'}
        message={`Por favor espere mientras se borra${plural(selected.length, 'n')} la${plural(selected.length)} proforma${plural(selected.length)} seleccionada${plural(selected.length)}`}
        open={waitDialogOpen}
      />

      <InformationDialog
        title={'Resultado'}
        messageArray={buildInformationMessageArray()}
        open={informationDialogOpen}
        onAccept={handleInformationDialogAccept}
        onClose={handleInformationDialogClose}
      />
    </div>
  );
        //message={`${successfullyReversed.length} fatura${plural(successfullyReversed.length)} anulada${plural(successfullyReversed.length)} exitósamente${successfullyReversed.length > 0 ? ':' : ''}\n${successfullyReversed}\n${failinglyReversed.length || 'Ninguna'} falla${plural(failinglyReversed.length)}${failinglyReversed.length > 0 ? ':' : ''}\n${failinglyReversed}`}
}
