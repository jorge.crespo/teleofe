import React, { Component } from 'react';
import _ from 'lodash';
import { today, createOptionForDate } from './DatePicker.js';
import { range } from './Util.js';
import moment from 'moment';
import ReactDOMServer from 'react-dom/server';

import ControlPanel from './ControlPanel.js';
import SimpleDialog from './SimpleDialog.js';
import {
  formasDePago,
} from './constants.js';

const formasDePagoWithLabels = _.pickBy(formasDePago, value => value.label != null);

/*
import {
  calculateTotal,
} from './helpers.js';
*/

import {
  rowCalcs,
} from './FactCalculations.js';

const ivaFactor = 0.12;

const retcodes = {
  SUCCE: 0,    // Success
  FETLN: 1,    // Fact equal to last number error
  FLTLN: 2,    // Fact less than last number error
  FUINM: 3,    // Fact used in next month error
  FGTNM: 4,    // Fact greather than next month error
  FUIPM: 5,    // Fact used in previous month error
  FLTPM: 6,    // Fact less than previous month error
  EBVIP: 26,   // Electronic billing verification in process
  EBVNS: 27,   // Electronic billing verification not yet started
  PRSUC: 100,  // Printing success
  PRERR: 101,  // Printing error
  CSSUC: 200,  // Customer success
  CSERR: 201,  // Customer error
};

function fixFactNum() {
  return this.fixFactNum();
}

function letTheUserFixIt() {
  return this.letTheUserFixIt();
}

const erroptions = [
  { code: retcodes.FETLN,
    options: [
      {message: 'Dejar que el sistema ponga un número de factura', action: fixFactNum},
      {message: 'Yo arreglo el número de factura', action: letTheUserFixIt},
    ]
  },
  { code: retcodes.FLTLN,
    options: [
      {message: 'Dejar que el sistema ponga un número de factura', action: fixFactNum},
      {message: 'Yo arreglo el número de factura', action: letTheUserFixIt},
    ]
  },
  { code: retcodes.FUINM,
    options: [
      {message: 'Dejar que el sistema intente arreglar el problema', action: fixFactNum},
      {message: 'Yo arreglo el número de factura', action: letTheUserFixIt},
    ]
  },
  { code: retcodes.FGTNM,
    options: [
      {message: 'Dejar que el sistema intente arreglar el problema', action: fixFactNum},
      {message: 'Yo arreglo el número de factura', action: letTheUserFixIt},
    ]
  },
  { code: retcodes.FUIPM,
    options: [
      {message: 'Dejar que el sistema intente arreglar el problema', action: fixFactNum},
      {message: 'Yo arreglo el número de factura', action: letTheUserFixIt},
    ]
  },
  { code: retcodes.FLTPM,
    options: [
      {message: 'Dejar que el sistema intente arreglar el problema', action: fixFactNum},
      {message: 'Yo arreglo el número de factura', action: letTheUserFixIt},
    ]
  },
];

const onPrintStates = {
  IDLE: 0,
  DONE: 1,
  ERROR: 2,
};

const N = 1;
const ELIM = 'ELIMINAR';
const FACT_NUM_LEN = 9;

const offlineSending = true;
const offlineVerification = true;

const { REACT_APP_API_URL: url } = process.env;

function fetchBusinessData() {
  return fetch(`${url}/business-data`)
    .then(response => response.json());
}

function htmlToPrint(accessKey, payload, businessData) {
  const rows = payload.rows
    .filter(row => row.article !== null)
    .map((row, index) => (
      <tr key = {index}>
        <td valign={'top'} align={'right'}>{`${row.quantity} `}</td>
        <td valign={'top'} colSpan={2}>
          {!row.unit || row.unit.toLowerCase() === 'c/u'
            ? row.article
            : row.unit + ' ' + row.article}
        </td>
        <td valign={'top'} align={'right'}>{row.price}</td>
        <td valign={'top'} align={'right'}>{row.total}</td>
      </tr>
    ));
  let theNiceDate = payload.date.date.format('D [de] MMMM [del] YYYY');
  let customer = payload.customer || {};
  let reactToPrint = (
      <div style={{fontSize: '10px'}}>
        <p align={'center'}>
          {businessData.nombreComercialCompleto || businessData.nombreComercial}
        </p>
        <p align={'center'}>
          {`MATRIZ: ${businessData.direccion}`}
        </p>
        <p align={'center'}>
          {businessData.razonSocial}
        </p>
        <p align={'center'}>
          {`RUC: ${businessData.ruc}`}
        </p>
        <p>
          {`Factura N°: ${businessData.establecimiento}-${businessData.puntoEmision}-${payload.fact}`}
        </p>
        <p>
          {`N° Autorización / Clave de Acceso: ${accessKey}`}
        </p>
        <p>
          {`Fecha: ${theNiceDate}`}
        </p>
        <p style={{margin: '0'}}>
          {`Cliente: ${customer.name || 'Consumidor final'}`}
        </p>
        <p style={{margin: '0'}}>
          {customer.ruced ? `R.U.C./C.I.: ${customer.ruced}` : ''}
        </p>
        {customer.address && <p style={{margin: '0'}}>Dirección: {customer.address}</p>}
        {customer.phone && <p style={{margin: '0'}}>Teléfono: {customer.phone}</p>}
        {customer.email && <p style={{margin: '0'}}>email: {customer.email}</p>}
        <p style={{margin: '0'}} align={'center'}>{'--------------------------------'}</p>
        <table><tbody>
          <tr>
            <th>CANT.</th><th colSpan={2}>DESC.</th><th>PRECIO</th><th>TOTAL</th>
          </tr>
          {rows}
          <tr>
            <td colSpan={2}/>
            <td colSpan={2} align={'right'}>{''}</td>
            <td align={'right'}>{'_'.repeat(Math.max(5, payload.bigTotal.length))}</td>
          </tr>
          <tr>
            <td colSpan={2}/>
            <td colSpan={2} align={'right'}>{'Merc. con IVA'}</td>
            <td align={'right'}>{payload.totalWithIva}</td>
          </tr>
          <tr>
            <td colSpan={2}/>
            <td colSpan={2} align={'right'}>{'Descuento'}</td>
            <td align={'right'}>{payload.descuento}</td>
          </tr>
          <tr>
            <td colSpan={2}/>
            <td colSpan={2} align={'right'}>{'Sub Total'}</td>
            <td align={'right'}>{payload.subtotal}</td>
          </tr>
          <tr>
            <td colSpan={2}/>
            <td colSpan={2} align={'right'}>{'I.V.A. 0%'}</td>
            <td align={'right'}>{payload.zeroIva}</td>
          </tr>
          <tr>
            <td colSpan={2}/>
            <td colSpan={2} align={'right'}>{'I.V.A. 12%'}</td>
            <td align={'right'}>{payload.iva}</td>
          </tr>
          <tr>
            <td colSpan={2}/>
            <td colSpan={2} align={'right'}>{'TOTAL'}</td>
            <td align={'right'}>{payload.bigTotal}</td>
          </tr>
        </tbody></table>
      </div>
  );
  const rendered = ReactDOMServer.renderToStaticMarkup(reactToPrint);
  return `<html><body>${rendered}</body></html>`
}

class FacturaTab extends Component {

  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getEmptyRow = (id) => ({
    id,
    code: null,
    quantity: '1',
    article: null,
    unit: null,
    noIva: null,
    price: null,
    taxBase: null,
    discount: null,
    total: null
  });

  getInitialState = () => ({
    isNew: true,
    date: today,
    fact: null,
    rows: Array.from(new Array(N), (val,index) => this.getEmptyRow(index)),
    nextRowId: N,
    totalWithIva: '0.00',
    descuento: '0.00',
    subtotal: '0.00',
    zeroIva: '0.00',
    iva: '0.00',
    bigTotal: '0.00',
    customer: {
      name: null,
      ruced: null,
      address: null,
      phone: null,
      email: null
    },
    isNewCustomer: false,
    isUpdatedCustomer: false,
    afterPrintDialog: {
      open: false,
      message: '',
    },
    errcode: null,
    saveCustomerState: onPrintStates.IDLE,
    saveTransactionState: onPrintStates.IDLE,
    printingState: onPrintStates.IDLE,
    sendTransactionState: onPrintStates.IDLE,
    verifyTransactionState: onPrintStates.IDLE,
    facturaFilePath: null,
    facturaAccessKey: null,
    discountRate: null,
    guiaEstab: null,
    guiaPtoEmi: null,
    guiaRemision: null,
    pagos: [{ formaDePago: formasDePago.EFECTIVO.code, value: '0.00' }],
  });

  static async miniPrint(obj) {
    let { rows = [] } = obj;
    rows = rows.map(row => {
      const total = rowCalcs.calculateTotal(row.quantity, row.price);
      return { ...row, total };
    });
    const { ['Dirección']: address, ['Teléfono']: phone, ['Email']: email } = obj;
    const customer = {
      name: obj.CLIENTE,
      ruced: obj.RUCED,
      ..._.pickBy({ address, phone, email })
    };
    const m = moment(
      `${obj.ANIO}-${obj.MES}-${obj.DIA}`,
      `${obj.yearFormat}-${obj.monthFormat}-${obj.dayFormat}`
    );

    const date = createOptionForDate(m);
    const {
      secuencial: fact,
      nombreComercial,
      direccion,
      razonSocial,
      ruc,
      establecimiento,
      puntoEmision
    } = obj;

    const businessData = {
      nombreComercial,
      direccion,
      razonSocial,
      ruc,
      establecimiento,
      puntoEmision
    };

    const {
      accessKey,
      TOTAL: bigTotal,
      IVA_ZERO: zeroIva = '0.00',
      IVA: iva,
      SUBTOTAL: subtotal,
      DESCUENTO: descuento
    } = obj;

    const totalWithIva = (Number(subtotal) + Number(iva)).toFixed(2);

    const data = {
      fact,
      date,
      customer,
      rows,
      bigTotal,
      zeroIva,
      iva,
      subtotal,
      descuento,
      totalWithIva,
    };

    const html = htmlToPrint(accessKey, data, businessData);

    return fetch(`${process.env.REACT_APP_PRINTER_URL}/print-html/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        html
      })
    })
    .then((response) => {
      return response.json()
        .then(data => response.ok ? data : Promise.reject(data));
    });
  }

  readNextSequential = () => {
    return fetch(`${url}/fact?which=next`)
      .then(response => response.json())
      .then(data => {
        if (data && data.nextFact) {
          return data.nextFact === 'NONE'
            ? Promise.reject('No hay archivos donde se pueda leer el número de factura')
            : data.nextFact;
        } else {
          return Promise.reject('Los datos (data || data.nextFact) son nulos');
        }
      });
  }

  setSequential = (sequential) => {
    const seqRequest = (this.lastSeqRequest = {});
    return new Promise((resolve, reject) => {
      if (sequential == null) {
        this.readNextSequential()
          .then(sequential => {
            if (seqRequest !== this.lastSeqRequest) return;
            //delete this.lastSeqRequest;
            //this.setState({ sequential }, resolve);
            this.setState({ fact: sequential }, resolve);
          })
          .catch(err => {
            console.log('No se pudo leer el número de factura, err:', err);
          });
      } else {
        if (seqRequest !== this.lastSeqRequest) return;
        //delete this.lastSeqRequest;
        //this.setState({ sequential }, resolve);
        this.setState({ fact: sequential }, resolve);
      }
    });
  };

  componentDidMount = () => this.setSequential();

  onCustomerSelected = (customer) => {
    this.setState({ customer, isNewCustomer: false, isUpdatedCustomer: false });
  }

  onCustomerNameCreated = (input) => {
    this.setState(state => {
      // If isNewCustomer, then RucedSelect has already started the
      // customer creation, so keep the ruced and the rest of the attributes
      let customer = state.isNewCustomer
        ? { ...state.customer, name: input } // Keep ruced and all attrs
        : { name: input }; // Remove ruced and all attrs except name

      return {
        ...state,
        customer,
        isNewCustomer: true,
        isUpdatedCustomer: false
      };
    });
  }

  onCustomerRucedCreated = (input) => {
    this.setState(state => {
      // If isNewCustomer, then NameSelect has already started the
      // customer creation, so keep the name and the rest of the attributes
      let customer = state.isNewCustomer
        ? { ...state.customer, ruced: input } // Keep name attr
        : { ruced: input }; // Remove name and all attrs except ruced

      return {
        ...state,
        customer,
        isNewCustomer: true,
        isUpdatedCustomer: false
      };
    });
  }

  onAddressChanged = (address) => {
    this.setState(state => ({
      ...state,
      customer: { ...state.customer, address },
      isUpdatedCustomer: true
    }));
  }

  onPhoneChanged = (phone) => {
    this.setState(state => ({
      ...state,
      customer: { ...state.customer, phone },
      isUpdatedCustomer: true
    }));
  }

  onEmailChanged = (email) => {
    this.setState(state => ({
      ...state,
      customer: { ...state.customer, email },
      isUpdatedCustomer: true
    }));
  }

  static calculateTotalWithIva(newRows) {
    let totalWithIva = newRows.reduce(function (acum, row) {
      return acum + (row.noIva ? 0 : Number(row.total || 0));
    }, 0);

    return totalWithIva.toFixed(2);
  }

  static calculateSubtotal(newRows) {
    return newRows.reduce(function (acum, row) {
      return acum + (row.noIva ? 0 : Number(row.taxBase));
    }, 0).toFixed(2);
  }

  static calculateDescuento(newRows) {
    return newRows
      .reduce((acum, row) => acum + Number(row.discount), 0)
      .toFixed(2);
  }

  static calculateZeroIva(newRows) {
    let zeroIva = newRows.reduce(function (acum, row) {
      return acum + (row.noIva ? Number(row.total || 0) : 0);
    }, 0);

    return zeroIva.toFixed(2);
  }

  static calculateBigTotal(subtotal, iva, zeroIva) {
    return (Number(subtotal) + Number(iva) + Number(zeroIva)).toFixed(2);
  }

  static calcTotals(rows, totalWithIva, zeroIva, discountRate) {
    const subtotal = this.calculateSubtotal(rows);
    const descuento = this.calculateDescuento(rows);
    const iva = (
      discountRate !== null
        ? Number(subtotal) * ivaFactor
        : Number(totalWithIva) - Number(subtotal)
      ).toFixed(2);
    const bigTotal = this.calculateBigTotal(subtotal, iva, zeroIva);
    return { subtotal, descuento, iva, bigTotal };
  }

/*
  static calculateTaxBase(noIva, total, discountRate) {
    return noIva ? total : (
      discountRate !== null
        ? Number(total) * (1 - Number(discountRate)/100)
        : Number(total) / (1 + ivaFactor)
      ).toFixed(2);
  }

  static calculateDiscount(noIva, total, taxBase) {
    return noIva ? '0.00' : (Number(total) - Number(taxBase)).toFixed(2);
  }
 */

  onQuantityChanged = (rowId, quantity, total) => {
    this.setState(state => {
      let newRows = state.rows.map((row, i) => {
        if (i === rowId) {
          //const taxBase = this.constructor.calculateTaxBase(row.noIva, total, state.discountRate);
          const taxBase = rowCalcs.calculateTaxBase(row.noIva, total, ivaFactor, state.discountRate);
          const discount = rowCalcs.calculateDiscount(row.noIva, total, taxBase);
          return { ...row, quantity, taxBase, discount, total };
        }
        return { ...row };
      });

      const totalWithIva = this.constructor.calculateTotalWithIva(newRows);
      const zeroIva = this.constructor.calculateZeroIva(newRows);
      return {
        ...state,
        rows: newRows,
        totalWithIva,
        zeroIva,
        ...this.constructor.calcTotals(
          newRows,
          totalWithIva,
          zeroIva,
          state.discountRate),
      };
    });
  }

  onProductChanged = (rowId, code, article, unit, noIva, price, total) => {
    this.setState(state => {
      let newRows = [];
      if (code === ELIM) {
        newRows = state.rows.filter((row, i) => rowId !== i);
      } else {
        newRows = state.rows.map((row, i) => {
          if (i === rowId) {
            //const taxBase = this.constructor.calculateTaxBase(noIva, total, state.discountRate);
            const taxBase = rowCalcs.calculateTaxBase(noIva, total, ivaFactor, state.discountRate);
            const discount = rowCalcs.calculateDiscount(noIva, total, taxBase);
            return {
              ...row,
              code,
              article,
              unit,
              noIva,
              price,
              taxBase,
              discount,
              total,
            };
          }
          return { ...row };
        });
      }

      const totalWithIva = this.constructor.calculateTotalWithIva(newRows);
      const zeroIva = this.constructor.calculateZeroIva(newRows);
      return {
        ...state,
        rows: newRows,
        totalWithIva,
        zeroIva,
        ...this.constructor.calcTotals(
          newRows,
          totalWithIva,
          zeroIva,
          state.discountRate),
      };
    });
  }

  pushEmptyRow = () => {
    this.setState(state => {
      let row = this.getEmptyRow(state.nextRowId);
      return {...state, nextRowId: state.nextRowId + 1, rows: state.rows.concat(row)};
    });
  }

  pushEmptyRows = (n) => {
    return new Promise(resolve => {
      this.setState(state => {
        const start = state.nextRowId;
        const end = start + n;
        const newRows = range(start, end).map(rowId => this.getEmptyRow(rowId));
        return {...state, nextRowId: end, rows: state.rows.concat(newRows)};
      }, resolve);
    });
  }

  saveCustomer = () => {
    const {
      isNewCustomer,
      isUpdatedCustomer,
      customer: newCustomer,
    } = this.state;

    console.log('*** Saving customer, isNewCustomer:', isNewCustomer, 'isUpdatedCustomer:', isUpdatedCustomer);

    if (!isNewCustomer && !isUpdatedCustomer) {
      return Promise.resolve(false);
    }

    const _url = isNewCustomer
      ? `${url}/new-customer/`
      : `${url}/update-customer/`;

    return fetch(_url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        customer: newCustomer
      })
    })
    .then(response => response.json()
      .then(data => response.ok ? data : Promise.reject(data))
    );
  };

  saveTransaction = () => {
    const {
      isNew,
      fact,
      customer,
      discountRate,
      guiaEstab,
      guiaPtoEmi,
      guiaRemision,
      date: { date }
    } = this.state;

    if (fact == null) {
      return Promise.reject({message: 'Ingrese un número de factura por favor'});
    }

    let dayFormat = 'D';
    let monthFormat = 'M';
    let yearFormat = 'YYYY';

    let transaction = {
      RUCED: customer.ruced,
      CLIENTE: customer.name,
      TOTAL_CON_IVA: this.state.totalWithIva,
      DESCUENTO: this.state.descuento,
      SUBTOTAL: this.state.subtotal,
      IVA: this.state.iva,
      IVA_ZERO: this.state.zeroIva,
      TOTAL: this.state.bigTotal,
      DIA: date.format(dayFormat),
      MES: date.format(monthFormat),
      ANIO: date.format(yearFormat),
      dayFormat,
      monthFormat,
      yearFormat,
      discountRate,
      guiaEstab,
      guiaPtoEmi,
      guiaRemision,
      address: customer.address,
      phone: customer.phone,
      email: customer.email,
      rows: this.state.rows.filter(row => row.code !== null),
    };

    const method = isNew ? 'POST' : 'PUT';

    return fetch(`${url}/save-transaction/`, {
      method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        transaction,
        factura: { number: fact },
      })
    })
    .then((response) => {
      return response.json()
        .then(data => response.ok ? data : Promise.reject(data));
    });
  };

  print = async (accessKey) => {
    const businessData = await fetchBusinessData();
    let html = htmlToPrint(accessKey, this.state, businessData);

    return fetch(`${process.env.REACT_APP_PRINTER_URL}/print-html/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        html
      })
    })
    .then((response) => {
      return response.json()
        .then(data => response.ok ? data : Promise.reject(data));
    });
  };

  sendTransaction = (filePath) => {
    return fetch(`${url}/send-transaction/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        filePath
      })
    })
    .then((response) => {
      return response.json()
        .then(data => response.ok ? data : Promise.reject(data));
    });
  };

  verifyTransaction = (filePath) => {
    return fetch(`${url}/verify-transaction/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        filePath
      })
    })
    .then((response) => {
      return response.json()
        .then(data => response.ok ? data : Promise.reject(data));
    });
  };

  // The onPrintStates must work as follows:
  //   If saveCustomerState is not DONE:
  //     then none of save, send, verify transaction states are DONE AND printingState is not DONE.
  //   If saveTransactionState is not DONE:
  //     then none of send, verify transaction states are DONE AND printingState is not DONE.
  //   If printingState is not DONE:
  //     then none of send, verify transaction states are DONE.
  //   If sendTransactionState is not DONE:
  //     then verifyTransactionState is not DONE.
  // Please make sure these constraints always hold.
  onPrintClicked = (evt) =>
    (function() {
      const {
        saveCustomerState,
        saveTransactionState,
        printingState,
        sendTransactionState,
        verifyTransactionState
      } = this.state;

      console.log('BEFORE EVERYTHING, states:',
        'saveCustomerState:', saveCustomerState,
        'saveTransactionState:', saveTransactionState,
        'printingState:', printingState,
        'sendTransactionState:', sendTransactionState,
        'verifyTransactionState:', verifyTransactionState);

      if (this.state.saveCustomerState !== onPrintStates.DONE) {
        return new Promise((resolve, reject) => {
          this.saveCustomer()
            .then((result) => {
              this.setState({
                saveCustomerState: onPrintStates.DONE
              }, () => resolve(result));
            })
            .catch((err) => {
              this.setState({
                saveCustomerState: onPrintStates.ERROR
              }, () => reject(err));
            });
        });
      } else {
        return Promise.resolve(true);
      }
    }).call(this)
    .then((response) => {
      if (this.state.saveTransactionState !== onPrintStates.DONE) {
        return new Promise((resolve, reject) => {
          this.saveTransaction()
            .then((result) => {
              this.setState({
                saveTransactionState: onPrintStates.DONE,
                facturaAccessKey: result.accessKey,
                facturaFilePath: result.message,
              }, () => resolve(result));  // result.message is the filePath
            })
            .catch((err) => {
              this.setState({
                saveTransactionState: onPrintStates.ERROR
              }, () => reject(err));
            });
        });
      } else {
        //const { fact, date: { date } } = this.state;
        const result = {
          code: retcodes.SUCCESS,
          accessKey: this.state.facturaAccessKey,
          message: this.state.facturaFilePath,
        };
        console.log('Saving is already DONE, resolving:', result);
        return Promise.resolve(result);
      }
    })
    .then((response) => {
      if (this.state.printingState !== onPrintStates.DONE) {
        return new Promise((resolve, reject) => {
          this.print(response.accessKey)
            .then((result) => {
              this.setState({
                printingState: onPrintStates.DONE
              }, () => resolve(response));  // response.message is the filePath
            })
            .catch((err) => {
              this.setState({
                printingState: onPrintStates.ERROR
              }, () => reject(err));
            });
        });
      } else {
        console.log('Printing is already DONE, resolving response:', response);
        return Promise.resolve(response);
      }
    })
    .then((response) => {
      if (this.state.sendTransactionState !== onPrintStates.DONE) {
        return new Promise((resolve, reject) => {
          if (offlineSending) {
            return resolve(response);
          }
          this.sendTransaction(response.message)
            .then((result) => {
              this.setState({
                sendTransactionState: onPrintStates.DONE
              }, () => resolve(response));
            })
            .catch((err) => {
              this.setState({
                sendTransactionState: onPrintStates.ERROR
              }, () => reject(err));
            });
        });
      } else {
        console.log('Sending is already DONE, resolving response:', response);
        return Promise.resolve(response);
      }
    })
    .then((response) => {
      if (this.state.verifyTransactionState !== onPrintStates.DONE) {
        return new Promise((resolve, reject) => {
          if (offlineSending || offlineVerification) {
            return resolve(response);
          }
          this.verifyTransaction(response.message)
            .then((result) => {
              this.setState({
                verifyTransactionState: onPrintStates.DONE
              }, () => resolve(response));
            })
            .catch((err) => {
              if (err.code === retcodes.EBVIP || err.code === retcodes.EBVNS) {
                console.log('Verification of authorization not finished yet:', err.message);
                this.setState({
                  verifyTransactionState: onPrintStates.DONE
                }, () => resolve(err));
              } else {
                this.setState({
                  verifyTransactionState: onPrintStates.ERROR
                }, () => reject(err));
              }
            });
        });
      } else {
        console.log('Verification is already DONE, resolving response:', response);
        return Promise.resolve(response);
      }
    })
    .then((response) => {
      this.setState({ errcode: null });
      console.log('BEFORE RETURNING OK, response:', response, 'errcode is:', this.state.errcode);
      return 'OK';
    })
    .catch((err) => {
      console.error('onPrintClicked, catched err:', err);

      const {
        saveCustomerState,
        saveTransactionState,
        printingState,
        sendTransactionState,
        verifyTransactionState
      } = this.state;

      console.log('IN THE CATCH, states:',
        'saveCustomerState:', saveCustomerState,
        'saveTransactionState:', saveTransactionState,
        'printingState:', printingState,
        'sendTransactionState:', sendTransactionState,
        'verifyTransactionState:', verifyTransactionState);

      let message = err.message;
      if (err.code === undefined) {
        if (saveCustomerState === onPrintStates.ERROR) {
          message = `Error while saving the new customer, message: ${err.message}. Please check the service is up.`;
        } else if (saveTransactionState === onPrintStates.ERROR) {
          message = `Error while saving the transaction, message: ${err.message}. Please check the service is up.`;
        } else if (sendTransactionState === onPrintStates.ERROR) {
          message = `Error while sending the transaction, message: ${err.message}. Please check the service is up.`;
        } else if (verifyTransactionState === onPrintStates.ERROR) {
          message = `Error while verifying the transaction, message: ${err.message}. Please check the service is up.`;
        } else {
          message = `Error while printing, message: ${err.message}. Please check the printing service is up.`;
        }
      }
      console.error(message);

      this.setState({
        errcode: err.code,
        afterPrintDialog: { open: true, message },
      });
      console.log('The rejected err is:', err);

      return Promise.reject(err);
    });

  onAfterPrintDialogClose = (option) => {
    this.setState(state => ({
      afterPrintDialog: { ...state.afterPrintDialog, open: false },
    }));
    if (option === null) {
      return Promise.resolve(true);
    }
    return option.action.call(this);
  }

  onSuccessClicked = () => {
    this.setState(state => ({ ...this.getInitialState(), checked: state.checked }));
    this.componentDidMount();
    this.controlPanelRef.componentDidMount();
    //window.location.reload();
  }

  printDisabled = () => {
    const invalidRows = this.state.rows.filter(row => row.code === null);

    return this.state.customer.ruced == null
      || this.state.customer.name == null
      || this.state.rows.length === 0
      || invalidRows.length === this.state.rows.length
      || this.state.fact === null;
  }

  getOptionsForRetcode = (retcode) => {
    if (!retcode) {
      return [];
    }
    const obj = erroptions.find(obj => obj.code === retcode);
    return obj == null ? [] : obj.options;
  }

  fixFactNum = () => {
    return this.readNextSequential()
      .then(fact => {
        this.setState({ fact, date: today });
        return Promise.resolve(true);
      })
      .catch(err => {
        console.log('No se pudo leer el numero de factura, err:', err);
        return Promise.reject(err);
      });
  }

  letTheUserFixIt = () => {
    return Promise.resolve(true);
  }

  onDiscountChanged = (discountRate) => {
    this.setState(state => {
      const rows = state.rows.map(row => {
        //const taxBase = this.constructor.calculateTaxBase(row.noIva, row.total, discountRate);
        const taxBase = rowCalcs.calculateTaxBase(row.noIva, row.total, ivaFactor, discountRate);
        const discount = rowCalcs.calculateDiscount(row.noIva, row.total, taxBase);

        return { ...row, taxBase, discount };
      });

      return {
        ...state,
        rows,
        discountRate,
        ...this.constructor.calcTotals(
          rows,
          state.totalWithIva,
          state.zeroIva,
          discountRate),
      };
    });
  }

  onFactChanged = (fact) => {
    if (fact.length > FACT_NUM_LEN) {
      fact = fact.slice(-FACT_NUM_LEN);
    }
    this.setState({ fact: fact.padStart(FACT_NUM_LEN, '0') });
  }

  onDiscountRateChanged = (val) => {
    if (val == null) {
      this.onDiscountChanged(null);
      return;
    }

    const discountRate = (val === '' ? '0' : val)
      .replace(/^0+([1-9])/,"$1")
      .replace(/^0+\.{1}([0-9])/,"0.$1");

    this.onDiscountChanged(discountRate);
  }

  onDiscountRateEnabled = () => {
    this.onDiscountChanged('15');
  }

  onDiscountRateDisabled = () => {
    this.onDiscountChanged(null);
  }

  setGuiaRemision = (guiaEstab, guiaPtoEmi, guiaRemision) =>
    new Promise(resolve => this.setState({ guiaEstab, guiaPtoEmi, guiaRemision }, resolve));

  facturar = async (guiaObj) => {
    const {
      establecimiento: guiaEstab,
      puntoEmision: guiaPtoEmi,
      secuencial: guiaRemision,
      ...obj
    } = guiaObj;

    if (guiaEstab != null && guiaPtoEmi != null && guiaRemision != null) {
      await this.setGuiaRemision(guiaEstab, guiaPtoEmi, guiaRemision);
    }

    await this.pushEmptyRows(obj.rows.length - this.state.rows.length);

    this.setState(state => ({
      ...state,
      rows: state.rows.map((row, index) => ({ ...row, quantity: obj.rows[index].quantity }))
    }));

    this.controlPanelRef.setCustomer(obj.RUCED);

    obj.rows.forEach((row, index) => {
      this.controlPanelRef.setProduct(index, row.code);
    });
  }

  edit = async (obj) => {
    this.setState({ isNew: false });
    await this.setSequential(obj.secuencial);

    this.controlPanelRef.setCustomer(obj.RUCED);

    const date = moment(
      `${obj.ANIO}-${obj.MES}-${obj.DIA}`,
      `${obj.yearFormat}-${obj.monthFormat}-${obj.dayFormat}`
    );

    this.setState({ date: createOptionForDate(date) });
    await this.pushEmptyRows(obj.rows.length - this.state.rows.length);

    this.setState(state => ({
      ...state,
      rows: state.rows.map((row, index) => ({ ...row, quantity: obj.rows[index].quantity }))
    }));

    obj.rows.forEach((row, index) => {
      this.controlPanelRef.setProduct(index, row.code);
    });
  }

  handleFormaDePagoChange = (oldCode, newCode) => {
    if (newCode === formasDePago.EFECTIVO.code) {
console.log('********* handleFormaDePagoChange: trying to change to efectivo!, doing nothing...');
      return;
    }
    if (oldCode === formasDePago.EFECTIVO.code) {
      this.setState(state => {
        const efectivo = state.pagos.find(p => p.formaDePago == formasDePago.EFECTIVO.code)
        const withoutEfectivo = state.pagos.filter(p => p.formaDePago != formasDePago.EFECTIVO.code)
        const newFormaDePago = Object.values(formasDePago).find(fp => fp.code === newCode);
        return {
          ...state,
          pagos: [
            ...withoutEfectivo,
            { formaDePago: newFormaDePago.code, value: efectivo.value },
            { formaDePago: efectivo.formaDePago, value: '0.00' }
          ]
        };
      });
    } else {
      this.setState(state => {
        const index = _.findIndex(state.pagos, p => p.formaDePago === oldCode);
        return {
          ...state,
          pagos: [
            ...state.pagos.slice(0, index),
            { formaDePago: newCode, value: state.pagos[index].value },
            ...state.pagos.slice(index + 1)
          ]
        };
      });
    }
  }

  handlePagoValueChange = (code, newValue) => {
console.log('************* handlePagoValueChange, code:', code, 'newValue:', newValue);
    this.setState(state => {
      const index = _.findIndex(state.pagos, p => p.formaDePago === code);
      return {
        ...state,
        pagos: [
          ...state.pagos.slice(0, index),
          { formaDePago: code, value: newValue },
          ...state.pagos.slice(index + 1)
        ]
      };
    });
  }

  render() {
    console.log('FacturaTab React version', React.version);

    const {
      errcode,
      fact,
      afterPrintDialog: { open, message },
      saveCustomerState,
      saveTransactionState,
    } = this.state;

    const isTransactionDisabled = (saveTransactionState === onPrintStates.DONE);
    const props = [
      'date',
      'isNewCustomer',
      'customer',
      'rows',
      'totalWithIva',
      'descuento',
      'subtotal',
      'zeroIva',
      'iva',
      'bigTotal',
      'discountRate',
      'guiaRemision',
      'pagos',
    ].reduce((acum, a) => ({...acum, [a]: this.state[a]}), {});

    return (
      <div>
        <ControlPanel
          ref = {e => this.controlPanelRef = e}
          sequential={fact}
          sequentialLabel={'Factura'}
          printDisabled={this.printDisabled()}
          onPrintClicked={this.onPrintClicked}
          onSuccessClicked={this.onSuccessClicked}
          onSequentialChanged = {this.onFactChanged}
          onDateChanged = {date => this.setState({ date })}
          onCustomerSelected = {this.onCustomerSelected}
          onCustomerNameCreated = {this.onCustomerNameCreated}
          onCustomerRucedCreated = {this.onCustomerRucedCreated}
          onQuantityChanged = {this.onQuantityChanged}
          onProductChanged = {this.onProductChanged}
          pushEmptyRow = {this.pushEmptyRow}
          onAddressChanged = {this.onAddressChanged}
          onPhoneChanged = {this.onPhoneChanged}
          onEmailChanged = {this.onEmailChanged}
          {...props}
          //settingsChecked = {checked}
          ELIM = {ELIM}
          isCustomerDisabled = {saveCustomerState === onPrintStates.DONE}
          isTransactionDisabled = {isTransactionDisabled}
          onDiscountRateChanged = {this.onDiscountRateChanged}
          onDiscountRateEnabled = {this.onDiscountRateEnabled}
          onDiscountRateDisabled = {this.onDiscountRateDisabled}
          onResetSequential = {this.setSequential}
          formasDePago = {formasDePagoWithLabels}
          onFormaDePagoChanged = {this.handleFormaDePagoChange}
          onPagoValueChanged = {this.handlePagoValueChange}
        />
        <SimpleDialog
          disableEscapeKeyDown
          message={message}
          open={open}
          options={this.getOptionsForRetcode(errcode)}
          onClose={this.onAfterPrintDialogClose}
        />
      </div>
    );
  }
}

export default FacturaTab;
