import React, { Component } from 'react';

export default class MainMenu extends Component {

    inputOpenFileRef;

    constructor(props) {
        super(props)
        this.inputOpenFileRef = React.createRef();
    }

    showOpenFileDlg = () => {
        this.inputOpenFileRef.current.click();
    }

    handleFileChange = (e) => {
      if (!e.target.files) {
        return;
      }

      //setFile(e.target.files[0]);
      console.log('************* e.target.files[0]:', e.target.files[0]);
    }

    render() {
        return (
            <div>
                <input
                  ref={this.inputOpenFileRef}
                  type="file"
                  style={{ display: "none" }}
                  onChange={this.handleFileChange}
                />
                <button onClick={this.showOpenFileDlg}>Open</button>
            </div>
        );
    }
}
