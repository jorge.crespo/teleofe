import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function InformationDialog(props) {

  const { title, messageArray, onAccept, onClose, ...other } = props;

  function handleAccept() {
    onAccept();
  }

  function handleClose(event, reason) {
    if (onClose)
      onClose(event, reason);
  }

  return (
    <Dialog
      {...other}
      onClose={handleClose}
      aria-labelledby="info-dialog-title"
      aria-describedby="info-dialog-description"
    >
      <DialogTitle id="info-dialog-title">{title}</DialogTitle>
      <DialogContent>
        {
          messageArray.map((msg, index) => (
            <DialogContentText
              key={`info-dialog-description-key${index}`}
              id={`info-dialog-description-id${index}`}
            >
              {msg}
            </DialogContentText>
          ))
        }
      </DialogContent>
      <DialogActions>
        <Button onClick={handleAccept} color="primary" autoFocus>
          Aceptar
        </Button>
      </DialogActions>
    </Dialog>
  );
}
