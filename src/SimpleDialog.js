import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import blue from '@material-ui/core/colors/blue';
import red from '@material-ui/core/colors/red';

import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DialogActions from '@material-ui/core/DialogActions';

const styles = theme => ({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  divider: {
    height: theme.spacing(2),
  },
  title: {
    color: red[700],
    //color: theme.palette.text.primary,
  },
});

class SimpleDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null,
    };
  }

  handleOk = () => {
    this.setState({ value: null });
    this.props.onClose(this.state.value);
  };

  handleChange = (_, value) => {
    this.setState({ value });
  };

  handleClose = (_, reason) => {
    if (reason !== 'backdropClick') {
      this.props.onClose(null);
    }
  };

  render() {
    const { classes, message, options, onClose, ...other } = this.props;
    const { value } = this.state;

    let msg = message || '';
    if (message != null && message.slice(-1) !== '.') {
      msg += '.';
    }
    const showOptions = options.length > 0;
    const noOptions = !showOptions;
    const Title = (props) => <div className={props.className}>{props.children}</div>;
            //value={!this.state.value || this.state.value.message}
    return (
      <Dialog
        aria-labelledby="confirmation-dialog-title"
        onClose={this.handleClose}
        {...other}>
        <DialogTitle id="confirmation-dialog-title">
          <Title className={classes.title}>Error</Title>
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {msg}
          </DialogContentText>
          {showOptions && <div className={classes.divider} />}
          {showOptions && (
          <DialogContentText id="alert-dialog-suggestion">
            Seleccione una de las siguientes alternativas:
          </DialogContentText>)}
          <RadioGroup
            ref={ref => {
              this.radioGroupRef = ref;
            }}
            aria-label="Error"
            name="error"
            value={value ? value.message : null}
          >
            {options.map(option => {
              const message = option.message;
              return (
                <FormControlLabel
                  value={message}
                  key={message}
                  control={<Radio />}
                  label={message}
                  onChange={(event) => this.handleChange(event, option)}
                />
              );
            })}
          </RadioGroup>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={this.handleOk}
            disabled={!noOptions && this.state.value === null}
            color="primary"
          >
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

SimpleDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  value: PropTypes.string,
};

export default withStyles(styles, { withTheme: true })(SimpleDialog);
