import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
//import ComposedTextField from './ComposedTextField.js';
import InputAdornment from '@material-ui/core/InputAdornment';

export default class ValueLabelColumn extends React.Component
{
  endAdornment = () => <InputAdornment position="start">%</InputAdornment>;
/*
  constructor(props) {
    super(props);
    //this.state = { formasDePago: null };
  }
*/
  handleChange = (oldFormaDePago, newFormaDePago) => {
    console.log('*********** FormaPagoColumn, handlechange, newFormaDePago:', newFormaDePago);
    this.props.onFormaDePagoChanged(oldFormaDePago, newFormaDePago);
  }

  render() {
    const { style = {}, readOnly = false, formasDePago, pago } = this.props;
    console.log('*********** FormaPagoColumn, render, pago:', pago);

/*
    if (this.state.formasDePago == null) {
      this.setState({ formasDePago });
    }
*/

    const formasDePagoKeys = Object.keys(formasDePago);

      //<td className = {'Product-row-label'} style = {{...style, border: '0px solid white'}}>
    return (
      <td className = {'Product-row-label'} colSpan = {this.props.colSpan || 1} style = {{...style, border: '0px solid white'}}>
        <div style = {{ display: 'flex', flexWrap: 'wrap', width: '100%' }} >
          <FormControl style = {{ width: '40%' }}>
            <InputLabel id="forma-pago-select-label">Forma Pagito</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={pago.formaDePago}
              label="Forma de Pago"
              onChange={event => this.handleChange(pago.formaDePago, event.target.value)}
            >
            {
              formasDePagoKeys.map(key => {
                const fp = formasDePago[key];
                return (<MenuItem key={key} value={fp.code}>{fp.label}</MenuItem>);
              })
            }
            </Select>
          </FormControl>
          {/*
            readOnly && <label id = {this.props.id} onDoubleClick = {this.props.onDoubleClick}>
              {this.props.content}
            </label>
          }
          {
            !readOnly && <ComposedTextField
              onLabelDoubleClick = {this.props.onLabelDoubleClick}
              label = {this.props.content}
              value = {this.props.inputValue}
              onChange = {this.props.onInputValueChanged}
              readOnly = {false}
              inputProps = {{
                endAdornment: this.endAdornment()
              }}
              disabled = {this.props.inputDisabled}
            />
          */}
          <FormControl style = {{ marginLeft: '20px', align: 'right' }}>
            <InputLabel
              htmlFor="component-simple"
            >
              {'Todita'}
            </InputLabel>
            <Input
              id="component-simple"
              readOnly={readOnly}
              disableUnderline={readOnly}
              value={pago.value}
              onChange={event => this.props.onValueChanged(pago.formaDePago, event.target.value)}
              disabled={this.props.disabled}
              {...this.props.inputProps}
            />
          </FormControl>
        </div>
      </td>
    );
  }
}
