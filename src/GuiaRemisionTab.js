import React, { Component } from 'react';
import { today, createOptionForDate } from './DatePicker.js';
import { range } from './Util.js';
import moment from 'moment';
import ReactDOMServer from 'react-dom/server';

import ControlPanel from './GuiaControlPanel.js';

import SimpleDialog from './SimpleDialog.js';

const { REACT_APP_API_URL: url } = process.env;

const ivaFactor = 0.12;

const retcodes = {
  SUCCE: 0,    // Success
  FETLN: 1,    // Fact equal to last number error
  FLTLN: 2,    // Fact less than last number error
  FUINM: 3,    // Fact used in next month error
  FGTNM: 4,    // Fact greather than next month error
  FUIPM: 5,    // Fact used in previous month error
  FLTPM: 6,    // Fact less than previous month error
  EBVIP: 26,   // Electronic billing verification in process
  EBVNS: 27,   // Electronic billing verification not yet started
  PRSUC: 100,  // Printing success
  PRERR: 101,  // Printing error
  CSSUC: 200,  // Customer success
  CSERR: 201,  // Customer error
};

function fixSequential() {
  return this.fixSequential();
}

function letTheUserFixIt() {
  return this.letTheUserFixIt();
}

const erroptions = [
  { code: retcodes.FETLN,
    options: [
      {message: 'Dejar que el sistema ponga un número de guía', action: fixSequential},
      {message: 'Yo arreglo el número de guía', action: letTheUserFixIt},
    ]
  },
  { code: retcodes.FLTLN,
    options: [
      {message: 'Dejar que el sistema ponga un número de guía', action: fixSequential},
      {message: 'Yo arreglo el número de guía', action: letTheUserFixIt},
    ]
  },
  { code: retcodes.FUINM,
    options: [
      {message: 'Dejar que el sistema intente arreglar el problema', action: fixSequential},
      {message: 'Yo arreglo el número de guía', action: letTheUserFixIt},
    ]
  },
  { code: retcodes.FGTNM,
    options: [
      {message: 'Dejar que el sistema intente arreglar el problema', action: fixSequential},
      {message: 'Yo arreglo el número de guía', action: letTheUserFixIt},
    ]
  },
  { code: retcodes.FUIPM,
    options: [
      {message: 'Dejar que el sistema intente arreglar el problema', action: fixSequential},
      {message: 'Yo arreglo el número de guía', action: letTheUserFixIt},
    ]
  },
  { code: retcodes.FLTPM,
    options: [
      {message: 'Dejar que el sistema intente arreglar el problema', action: fixSequential},
      {message: 'Yo arreglo el número de guía', action: letTheUserFixIt},
    ]
  },
];

const onPrintStates = {
  IDLE: 0,
  DONE: 1,
  ERROR: 2,
};

const N = 1;
const ELIM = 'ELIMINAR';
const FACT_NUM_LEN = 9;

const offlineSending = true;
const offlineVerification = true;

class GuiaRemisionTab extends Component {

  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getEmptyRow = (id) => ({
    id,
    code: null,
    quantity: '1',
    article: null,
    unit: null,
    noIva: null,
    price: null,
    taxBase: null,
    discount: null,
    total: null
  });

  getInitialState = () => ({
    isNew: true,
    date: today,
    endDate: today,
    sequential: null,
    rows: Array.from(new Array(N), (val,index) => this.getEmptyRow(index)),
    nextRowId: N,
    totalWithIva: '0.00',
    descuento: '0.00',
    subtotal: '0.00',
    zeroIva: '0.00',
    iva: '0.00',
    bigTotal: '0.00',
    customer: {
      name: null,
      ruced: null,
      address: null,
      phone: null,
      email: null
    },
    isNewCustomer: false,
    isUpdatedCustomer: false,
    transportista: {
      name: null,
      ruced: null,
      placa: null,
      destinationAddress: null,
    },
    isNewTransportista: false,
    isUpdatedTransportista: false,
    afterPrintDialog: {
      open: false,
      message: '',
    },
    errcode: null,
    saveCustomerState: onPrintStates.IDLE,
    saveTransportistaState: onPrintStates.IDLE,
    saveTransactionState: onPrintStates.IDLE,
    printingState: onPrintStates.IDLE,
    sendTransactionState: onPrintStates.IDLE,
    verifyTransactionState: onPrintStates.IDLE,
    facturaFilePath: null,
    facturaAccessKey: null,
    discountRate: null
  });

  readNextSequential = () => {
    return fetch(`${url}/guiar?which=next`)
      .then(response => response.json())
      .then(data => {
        if (data && data.nextGuiar) {
          return data.nextGuiar === 'NONE'
            ? Promise.reject('No hay archivos donde se pueda leer el número de guia de remisión')
            : data.nextGuiar;
        } else {
          return Promise.reject('Los datos (data || data.nextGuiar) son nulos');
        }
      });
  }

  setSequential = (sequential) => {
    const seqRequest = (this.lastSeqRequest = {});
    return new Promise((resolve, reject) => {
      if (sequential == null) {
        this.readNextSequential()
          .then(sequential => {
            if (seqRequest !== this.lastSeqRequest) return;
            //delete this.lastSeqRequest;
            this.setState({ sequential }, resolve);
          })
          .catch(err => {
            console.log('No se pudo leer el numero de guía, err:', err);
          });
      } else {
        if (seqRequest !== this.lastSeqRequest) return;
        //delete this.lastSeqRequest;
        this.setState({ sequential }, resolve);
      }
    });
  };

  componentDidMount = () => {
console.log('********** in componentDidMount guiaRemisionTab, resetting sequential');
    return this.setSequential();
  }

  onCustomerSelected = (customer) => {
console.log('********** GuiaRemisionTab, onCustomerSelected, customer:', customer);
    this.setState({ customer, isNewCustomer: false, isUpdatedCustomer: false });
  }

  onTransportistaSelected = (transportista) => {
    this.setState({ transportista, isNewTransportista: false, isUpdatedTransportista: false });
  }

  onCustomerNameCreated = (input) => {
    this.setState(state => {
      // If isNewCustomer, then RucedSelect has already started the
      // customer creation, so keep the ruced and the rest of the attributes
      let customer = state.isNewCustomer
        ? { ...state.customer, name: input } // Keep ruced and all attrs
        : { name: input }; // Remove ruced and all attrs except name

      return {
        ...state,
        customer,
        isNewCustomer: true,
        isUpdatedCustomer: false
      };
    });
  }

  onTransportistaNameCreated = (input) => {
    this.setState(state => {
      // If isNewTransportista, then TransportistaRucedSelect has already
      // started the transportista creation, so keep the ruced and the rest
      // of the attributes
      let transportista = state.isNewTransportista
        ? { ...state.transportista, name: input } // Keep ruced and all attrs
        : { name: input }; // Remove ruced and all attrs except name

      return {
        ...state,
        transportista,
        isNewTransportista: true,
        isUpdatedTransportista: false
      };
    });
  }

  onCustomerRucedCreated = (input) => {
    this.setState(state => {
      // If isNewCustomer, then NameSelect has already started the
      // customer creation, so keep the name and the rest of the attributes
      let customer = state.isNewCustomer
        ? { ...state.customer, ruced: input } // Keep name attr
        : { ruced: input }; // Remove name and all attrs except ruced

      return {
        ...state,
        customer,
        isNewCustomer: true,
        isUpdatedCustomer: false
      };
    });
  }

  onTransportistaRucedCreated = (input) => {
    this.setState(state => {
      // If isNewTransportista, then TransportistaNameSelect has already
      // started the customer creation, so keep the name and the rest of the
      // attributes
      let transportista = state.isNewTransportista
        ? { ...state.transportista, ruced: input } // Keep name attr
        : { ruced: input }; // Remove name and all attrs except ruced

      return {
        ...state,
        transportista,
        isNewTransportista: true,
        isUpdatedTransportista: false
      };
    });
  }

  onAddressChanged = (address) => {
    this.setState(state => ({
      ...state,
      customer: { ...state.customer, address },
      isUpdatedCustomer: true
    }));
  }

  onPhoneChanged = (phone) => {
    this.setState(state => ({
      ...state,
      customer: { ...state.customer, phone },
      isUpdatedCustomer: true
    }));
  }

  onEmailChanged = (email) => {
    this.setState(state => ({
      ...state,
      customer: { ...state.customer, email },
      isUpdatedCustomer: true
    }));
  }

  onPlacaChanged = placa => {
    this.setState(state => ({
      ...state,
      transportista: { ...state.transportista, placa },
      isUpdatedTransportista: true
    }));
  }

  onDestinationAddressChanged = (destinationAddress) => {
    this.setState(state => ({
      ...state,
      transportista: { ...state.transportista, destinationAddress },
      isUpdatedTransportista: true
    }));
  }

  static calculateTotalWithIva(newRows) {
    let totalWithIva = newRows.reduce(function (acum, row) {
      return acum + (row.noIva ? 0 : Number(row.total || 0));
    }, 0);

    return totalWithIva.toFixed(2);
  }

  static calculateSubtotal(newRows) {
    return newRows.reduce(function (acum, row) {
      return acum + (row.noIva ? 0 : Number(row.taxBase));
    }, 0).toFixed(2);
  }

  static calculateDescuento(newRows) {
    return newRows
      .reduce((acum, row) => acum + Number(row.discount), 0)
      .toFixed(2);
  }

  static calculateZeroIva(newRows) {
    let zeroIva = newRows.reduce(function (acum, row) {
      return acum + (row.noIva ? Number(row.total || 0) : 0);
    }, 0);

    return zeroIva.toFixed(2);
  }

  static calculateBigTotal(subtotal, iva, zeroIva) {
    return (Number(subtotal) + Number(iva) + Number(zeroIva)).toFixed(2);
  }

  static calcTotals(rows, totalWithIva, zeroIva, discountRate) {
    const subtotal = this.calculateSubtotal(rows);
    const descuento = this.calculateDescuento(rows);
    const iva = (
      discountRate !== null
        ? Number(subtotal) * ivaFactor
        : Number(totalWithIva) - Number(subtotal)
      ).toFixed(2);
    const bigTotal = this.calculateBigTotal(subtotal, iva, zeroIva);
    return { subtotal, descuento, iva, bigTotal };
  }

  static calculateTaxBase(noIva, total, discountRate) {
    return noIva ? total : (
      discountRate !== null
        ? Number(total) * (1 - Number(discountRate)/100)
        : Number(total) / (1 + ivaFactor)
      ).toFixed(2);
  }

  static calculateDiscount(noIva, total, taxBase) {
    return noIva ? '0.00' : (Number(total) - Number(taxBase)).toFixed(2);
  }

  onQuantityChanged = (rowId, quantity, total) => {
    this.setState(state => {
      let newRows = state.rows.map((row, i) => {
        if (i === rowId) {
          const taxBase = this.constructor.calculateTaxBase(row.noIva, total, state.discountRate);
          const discount = this.constructor.calculateDiscount(row.noIva, total, taxBase);
          return { ...row, quantity, taxBase, discount, total };
        }
        return { ...row };
      });

      const totalWithIva = this.constructor.calculateTotalWithIva(newRows);
      const zeroIva = this.constructor.calculateZeroIva(newRows);
      return {
        ...state,
        rows: newRows,
        totalWithIva,
        zeroIva,
        ...this.constructor.calcTotals(
          newRows,
          totalWithIva,
          zeroIva,
          state.discountRate),
      };
    });
  }

  onProductChanged = (rowId, code, article, unit, noIva, price, total) => {
    this.setState(state => {
      let newRows = [];
      if (code === ELIM) {
        newRows = state.rows.filter((row, i) => rowId !== i);
      } else {
        newRows = state.rows.map((row, i) => {
          if (i === rowId) {
            const taxBase = this.constructor.calculateTaxBase(noIva, total, state.discountRate);
            const discount = this.constructor.calculateDiscount(noIva, total, taxBase);
            return {
              ...row,
              code,
              article,
              unit,
              noIva,
              price,
              taxBase,
              discount,
              total,
            };
          }
          return { ...row };
        });
      }

      const totalWithIva = this.constructor.calculateTotalWithIva(newRows);
      const zeroIva = this.constructor.calculateZeroIva(newRows);
      return {
        ...state,
        rows: newRows,
        totalWithIva,
        zeroIva,
        ...this.constructor.calcTotals(
          newRows,
          totalWithIva,
          zeroIva,
          state.discountRate),
      };
    });
  }

  pushEmptyRow = () => {
    this.setState(state => {
      let row = this.getEmptyRow(state.nextRowId);
      return {...state, nextRowId: state.nextRowId + 1, rows: state.rows.concat(row)};
    }, () => console.log('********* in pushEmptyRow'));
  }

  pushEmptyRows = (n) => {
    return new Promise(resolve => {
      this.setState(state => {
        const start = state.nextRowId;
        const end = start + n;
        const newRows = range(start, end).map(rowId => this.getEmptyRow(rowId));
console.log('**** pushed start:', start, 'end:', end, 'n:', n, 'range(start, end):', range(start, end));
        return {...state, nextRowId: end, rows: state.rows.concat(newRows)};
      }, resolve);
    });
  }

  saveCustomer = () => {
    const {
      isNewCustomer,
      isUpdatedCustomer,
      customer: newCustomer,
    } = this.state;

    console.log('*** Saving customer, isNewCustomer:', isNewCustomer, 'isUpdatedCustomer:', isUpdatedCustomer);

    if (!isNewCustomer && !isUpdatedCustomer) {
      return Promise.resolve(false);
    }

    const _url = isNewCustomer
      ? `${url}/new-customer/`
      : `${url}/update-customer/`;

    return fetch(_url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        customer: newCustomer
      })
    })
    .then(response => response.json()
      .then(data => response.ok ? data : Promise.reject(data))
    );
  };

  saveTransportista = () => {
    const {
      isNewTransportista,
      isUpdatedTransportista,
      transportista: newTransportista,
    } = this.state;

    console.log('*** Saving Transportista, isNewTransportista:', isNewTransportista, 'isUpdatedTransportista:', isUpdatedTransportista);

    if (!isNewTransportista && !isUpdatedTransportista) {
      return Promise.resolve(false);
    }

    const _url = isNewTransportista
      ? `${url}/new-customer/`
      : `${url}/update-customer/`;

    return fetch(_url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        customer: newTransportista
      })
    })
    .then(response => response.json()
      .then(data => response.ok ? data : Promise.reject(data))
    );
  };

  saveTransaction = () => {
    const {
      isNew,
      sequential,
      customer,
      transportista,
      date: { date },
      endDate: { date: endDate },
    } = this.state;

    if (sequential == null) {
      return Promise.reject({message: 'Ingrese un número de guía de remisión por favor'});
    }

    const dayFormat = 'D';
    const monthFormat = 'M';
    const yearFormat = 'YYYY';

    const transaction = {
      RUCED: customer.ruced,
      CLIENTE: customer.name,
      DIA: date.format(dayFormat),
      MES: date.format(monthFormat),
      ANIO: date.format(yearFormat),
      dayFormat,
      monthFormat,
      yearFormat,
      startTransportationDate: {
        DIA: date.format(dayFormat),
        MES: date.format(monthFormat),
        ANIO: date.format(yearFormat),
        dayFormat,
        monthFormat,
        yearFormat,
      },
      endTransportationDate: {
        DIA: endDate.format(dayFormat),
        MES: endDate.format(monthFormat),
        ANIO: endDate.format(yearFormat),
        dayFormat,
        monthFormat,
        yearFormat,
      },
      address: customer.address,
      phone: customer.phone,
      email: customer.email,
      rows: this.state.rows.filter(row => row.code !== null),
      razonSocialTransportista: transportista.name,
      rucTransportista: transportista.ruced,
      placa: transportista.placa,
      dirDestinatario: transportista.destinationAddress,
    };

    const method = isNew ? 'POST' : 'PUT';

    return fetch(`${url}/save-guia-remision/`, {
      method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        transaction,
        guia_remision: { number: sequential },
      })
    })
    .then((response) => {
      return response.json()
        .then(data => response.ok ? data : Promise.reject(data));
    });
  };

  print = async (accessKey) => {
    return Promise.resolve(true);
/*
    let html = await this.htmlToPrint(accessKey);

    return fetch('http://localhost:3002/print-html/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        html
      })
    })
    .then((response) => {
      return response.json()
        .then(data => response.ok ? data : Promise.reject(data));
    });
 */
  };

  sendTransaction = (filePath) => {
    return fetch(`${url}/send-transaction/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        filePath
      })
    })
    .then((response) => {
      return response.json()
        .then(data => response.ok ? data : Promise.reject(data));
    });
  };

  verifyTransaction = (filePath) => {
    return fetch(`${url}/verify-transaction/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        filePath
      })
    })
    .then((response) => {
      return response.json()
        .then(data => response.ok ? data : Promise.reject(data));
    });
  };

  // The onPrintStates must work as follows:
  //   If saveCustomerState is not DONE:
  //     then none of transportista, save, send, verify transaction states are DONE AND printingState is not DONE.
  //   If saveTransportistaState is not DONE:
  //     then none of save, send, verify transaction states are DONE AND printingState is not DONE.
  //   If saveTransactionState is not DONE:
  //     then none of send, verify transaction states are DONE AND printingState is not DONE.
  //   If printingState is not DONE:
  //     then none of send, verify transaction states are DONE.
  //   If sendTransactionState is not DONE:
  //     then verifyTransactionState is not DONE.
  // Please make sure these constraints always hold.
  onPrintClicked = (evt) =>
    (function() {
      const {
        saveCustomerState,
        saveTransportistaState,
        saveTransactionState,
        printingState,
        sendTransactionState,
        verifyTransactionState
      } = this.state;

      console.log('BEFORE EVERYTHING, states:',
        'saveCustomerState:', saveCustomerState,
        'saveTransportistaState:', saveTransportistaState,
        'saveTransactionState:', saveTransactionState,
        'printingState:', printingState,
        'sendTransactionState:', sendTransactionState,
        'verifyTransactionState:', verifyTransactionState);

      if (this.state.saveCustomerState !== onPrintStates.DONE) {
        return new Promise((resolve, reject) => {
          this.saveCustomer()
            .then((result) => {
              this.setState({
                saveCustomerState: onPrintStates.DONE
              }, () => resolve(result));
            })
            .catch((err) => {
              this.setState({
                saveCustomerState: onPrintStates.ERROR
              }, () => reject(err));
            });
        });
      } else {
        return Promise.resolve(true);
      }
    }).call(this)
    .then((response) => {
      if (this.state.saveTransportistaState !== onPrintStates.DONE) {
        return new Promise((resolve, reject) => {
          this.saveTransportista()
            .then((result) => {
              this.setState({
                saveTransportistaState: onPrintStates.DONE
              }, () => resolve(result));
            })
            .catch((err) => {
              this.setState({
                saveTransportistaState: onPrintStates.ERROR
              }, () => reject(err));
            });
        });
      } else {
        return Promise.resolve(true);
      }
    })
    .then((response) => {
      if (this.state.saveTransactionState !== onPrintStates.DONE) {
        return new Promise((resolve, reject) => {
          this.saveTransaction()
            .then((result) => {
              this.setState({
                saveTransactionState: onPrintStates.DONE,
                facturaAccessKey: result.accessKey,
                facturaFilePath: result.message,
              }, () => resolve(result));  // result.message is the filePath
            })
            .catch((err) => {
              this.setState({
                saveTransactionState: onPrintStates.ERROR
              }, () => reject(err));
            });
        });
      } else {
        const result = {
          code: retcodes.SUCCESS,
          accessKey: this.state.facturaAccessKey,
          message: this.state.facturaFilePath,
        };
        console.log('Saving is already DONE, resolving:', result);
        return Promise.resolve(result);
      }
    })
    .then((response) => {
      if (this.state.printingState !== onPrintStates.DONE) {
        return new Promise((resolve, reject) => {
          this.print(response.accessKey)
            .then((result) => {
              this.setState({
                printingState: onPrintStates.DONE
              }, () => resolve(response));  // response.message is the filePath
            })
            .catch((err) => {
              this.setState({
                printingState: onPrintStates.ERROR
              }, () => reject(err));
            });
        });
      } else {
        console.log('Printing is already DONE, resolving response:', response);
        return Promise.resolve(response);
      }
    })
    .then((response) => {
      if (this.state.sendTransactionState !== onPrintStates.DONE) {
        return new Promise((resolve, reject) => {
          if (offlineSending) {
            return resolve(response);
          }
          this.sendTransaction(response.message)
            .then((result) => {
              this.setState({
                sendTransactionState: onPrintStates.DONE
              }, () => resolve(response));
            })
            .catch((err) => {
              this.setState({
                sendTransactionState: onPrintStates.ERROR
              }, () => reject(err));
            });
        });
      } else {
        console.log('Sending is already DONE, resolving response:', response);
        return Promise.resolve(response);
      }
    })
    .then((response) => {
      if (this.state.verifyTransactionState !== onPrintStates.DONE) {
        return new Promise((resolve, reject) => {
          if (offlineSending || offlineVerification) {
            return resolve(response);
          }
          this.verifyTransaction(response.message)
            .then((result) => {
              this.setState({
                verifyTransactionState: onPrintStates.DONE
              }, () => resolve(response));
            })
            .catch((err) => {
              if (err.code === retcodes.EBVIP || err.code === retcodes.EBVNS) {
                console.log('Verification of authorization not finished yet:', err.message);
                this.setState({
                  verifyTransactionState: onPrintStates.DONE
                }, () => resolve(err));
              } else {
                this.setState({
                  verifyTransactionState: onPrintStates.ERROR
                }, () => reject(err));
              }
            });
        });
      } else {
        console.log('Verification is already DONE, resolving response:', response);
        return Promise.resolve(response);
      }
    })
    .then((response) => {
      this.setState({errcode: null});
      console.log('BEFORE RETURNING OK, response:', response, 'errcode is:', this.state.errcode);
      return 'OK';
    })
    .catch((err) => {
      console.error('onPrintClicked, catched err:', err);

      const {
        saveCustomerState,
        saveTransactionState,
        printingState,
        sendTransactionState,
        verifyTransactionState
      } = this.state;

      console.log('IN THE CATCH, states:',
        'saveCustomerState:', saveCustomerState,
        'saveTransactionState:', saveTransactionState,
        'printingState:', printingState,
        'sendTransactionState:', sendTransactionState,
        'verifyTransactionState:', verifyTransactionState);

      let message = err.message;
      if (err.code === undefined) {
        if (saveCustomerState === onPrintStates.ERROR) {
          message = `Error while saving the new customer, message: ${err.message}. Please check the service is up.`;
        } else if (saveTransactionState === onPrintStates.ERROR) {
          message = `Error while saving the transaction, message: ${err.message}. Please check the service is up.`;
        } else if (sendTransactionState === onPrintStates.ERROR) {
          message = `Error while sending the transaction, message: ${err.message}. Please check the service is up.`;
        } else if (verifyTransactionState === onPrintStates.ERROR) {
          message = `Error while verifying the transaction, message: ${err.message}. Please check the service is up.`;
        } else {
          message = `Error while printing, message: ${err.message}. Please check the printing service is up.`;
        }
      }
      console.error(message);

      this.setState({
        errcode: err.code,
        afterPrintDialog: {open: true, message},
      });
      console.log('The rejected err is:', err);

      return Promise.reject(err);
    });

  onAfterPrintDialogClose = (option) => {
    this.setState(state => ({
      afterPrintDialog: {...state.afterPrintDialog, open: false},
    }));
    if (option === null) {
      return Promise.resolve(true);
    }
    return option.action.call(this);
  }

  onSuccessClicked = () => {
    this.setState(state => ({ ...this.getInitialState(), checked: state.checked }));
    this.componentDidMount();
    this.controlPanelRef.componentDidMount();
    //window.location.reload();
  }

  printDisabled = () => {
    const invalidRows = this.state.rows.filter(row => row.code === null);

    return this.state.customer.ruced == null
      || this.state.customer.name == null
      || this.state.transportista.ruced == null
      || this.state.transportista.name == null
      || this.state.rows.length === 0
      || invalidRows.length === this.state.rows.length;
      //|| this.state.sequential === null;
  }

  getOptionsForRetcode = (retcode) => {
    if (!retcode) {
      return [];
    }
    const obj = erroptions.find(obj => obj.code === retcode);
    return obj == null ? [] : obj.options;
  }

  fixSequential = () => {
    return this.readNextSequential()
      .then(sequential => {
        this.setState({ sequential, date: today });
        return Promise.resolve(true);
      })
      .catch(err => {
        console.log('No se pudo leer el numero de guía, err:', err);
        return Promise.reject(err);
      });
  }

  letTheUserFixIt = () => {
    return Promise.resolve(true);
  }

  onDiscountChanged = (discountRate) => {
    this.setState(state => {
      const rows = state.rows.map(row => {
        const taxBase = this.constructor.calculateTaxBase(row.noIva, row.total, discountRate);
        const discount = this.constructor.calculateDiscount(row.noIva, row.total, taxBase);

        return { ...row, taxBase, discount };
      });

      return {
        ...state,
        rows,
        discountRate,
        ...this.constructor.calcTotals(
          rows,
          state.totalWithIva,
          state.zeroIva,
          discountRate),
      };
    });
/*
    const { totalWithIva, zeroIva, rows } = this.state;
    this.setState({
      discountValue,
      ...this.constructor.calcTotals(rows, totalWithIva, zeroIva, discountValue),
    });
 */
  }

  onSequentialChanged = (sequential) => {
    if (sequential.length > FACT_NUM_LEN) {
      sequential = sequential.slice(-FACT_NUM_LEN);
    }
    this.setState({ sequential: sequential.padStart(FACT_NUM_LEN, '0') });
  }

  onDiscountRateChanged = (val) => {
    if (val == null) {
      this.onDiscountChanged(null);
      return;
    }

    const discountRate = (val === '' ? '0' : val)
      .replace(/^0+([1-9])/,"$1")
      .replace(/^0+\.{1}([0-9])/,"0.$1");

    this.onDiscountChanged(discountRate);
    //this.setState({ discountRate });
  }

  onDiscountRateEnabled = () => {
    //this.setState({ discountRate: '15' })
    this.onDiscountChanged('15');
  }

  onDiscountRateDisabled = () => {
    this.onDiscountChanged(null);
  }

  edit = async (obj) => {
console.log('******** in guia remision, setting sequential, obj:', obj);

    this.setState({ isNew: false });
    await this.setSequential(obj.secuencial);

    this.controlPanelRef.setCustomer(obj.RUCED);
    this.controlPanelRef.setTransportista(obj.rucTransportista);

    const { startTransportationDate: start } = obj;
    const date = moment(
      `${start.ANIO}-${start.MES}-${start.DIA}`,
      `${start.yearFormat}-${start.monthFormat}-${start.dayFormat}`
    );
console.log('************** GuiaRemisionTab, edit, date:', date);

    const { endTransportationDate: end } = obj;
    const endDate = moment(
      `${end.ANIO}-${end.MES}-${end.DIA}`,
      `${end.yearFormat}-${end.monthFormat}-${end.dayFormat}`
    );
console.log('************** GuiaRemisionTab, edit, endDate:', endDate);

    this.setState({ date: createOptionForDate(date), endDate: createOptionForDate(endDate) });
    await this.pushEmptyRows(obj.rows.length - this.state.rows.length);

    this.setState(state => ({
      ...state,
      rows: state.rows.map((row, index) => ({ ...row, quantity: obj.rows[index].quantity }))
    }));

    obj.rows.forEach((row, index) => {
      this.controlPanelRef.setProduct(index, row.code);
    });

  }

  render() {
    console.log('GuiaRemisionTab React version', React.version);

    const {
      errcode,
      afterPrintDialog: {open, message},
      saveCustomerState,
      saveTransactionState,
    } = this.state;

    const isTransactionDisabled = (saveTransactionState === onPrintStates.DONE);
    let props = [
      'sequential',
      'date',
      'endDate',
      'isNewCustomer',
      'customer',
      'isNewTransportista',
      'transportista',
      'rows',
      'totalWithIva',
      'descuento',
      'subtotal',
      'zeroIva',
      'iva',
      'bigTotal',
      'discountRate',
    ].reduce((acum, a) => ({...acum, [a]: this.state[a]}), {});

    return (
      <div>
        <ControlPanel
          ref = {e => this.controlPanelRef = e}
          sequentialLabel={'Guía de Remisión'}
          printDisabled={this.printDisabled()}
          onPrintClicked={this.onPrintClicked}
          onSuccessClicked={this.onSuccessClicked}
          onSequentialChanged = {this.onSequentialChanged}
          onDateChanged = {date => this.setState({ date })}
          onEndDateChanged = {endDate => this.setState({ endDate })}
          onCustomerSelected = {this.onCustomerSelected}
          onTransportistaSelected = {this.onTransportistaSelected}
          onCustomerNameCreated = {this.onCustomerNameCreated}
          onTransportistaNameCreated = {this.onTransportistaNameCreated}
          onCustomerRucedCreated = {this.onCustomerRucedCreated}
          onTransportistaRucedCreated = {this.onTransportistaRucedCreated}
          onQuantityChanged = {this.onQuantityChanged}
          onProductChanged = {this.onProductChanged}
          pushEmptyRow = {this.pushEmptyRow}
          onAddressChanged = {this.onAddressChanged}
          onPhoneChanged = {this.onPhoneChanged}
          onEmailChanged = {this.onEmailChanged}
          onPlacaChanged = {this.onPlacaChanged}
          onDestinationAddressChanged = {this.onDestinationAddressChanged}
          {...props}
          //settingsChecked = {checked}
          ELIM = {ELIM}
          isCustomerDisabled = {saveCustomerState === onPrintStates.DONE}
          isTransactionDisabled = {isTransactionDisabled}
          onDiscountRateChanged = {this.onDiscountRateChanged}
          onDiscountRateEnabled = {this.onDiscountRateEnabled}
          onDiscountRateDisabled = {this.onDiscountRateDisabled}
          onResetSequential = {this.setSequential}
        />
        <SimpleDialog
          disableEscapeKeyDown
          message={message}
          open={open}
          options={this.getOptionsForRetcode(errcode)}
          onClose={this.onAfterPrintDialogClose}
        />
      </div>
    );
  }
}

export default GuiaRemisionTab;
