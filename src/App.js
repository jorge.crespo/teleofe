import React, { Component } from 'react';
import { today } from './DatePicker.js';
import './App.css';
import ReactDOMServer from 'react-dom/server';

import AppBar from './AppBar.js';
import Drawer from './CreateFacturaDrawer.js';
import FacturaTab from './FacturaTab.js';
import GuiaRemisionTab from './GuiaRemisionTab.js';
import ProformaTab from './ProformaTab.js';
import SimpleTable from './SimpleTableHead.js';
import TheTable from './EnhancedTableHead.js';

const { REACT_APP_API_URL: url } = process.env;

class App extends Component {

  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState = () => ({
    drawerOpen: false,
    tabIndex: 0,
  });

  sendTransaction = (filePath) => {
    return fetch(`${url}/send-transaction/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        filePath
      })
    })
    .then((response) => {
      return response.json()
        .then(data => response.ok ? data : Promise.reject(data));
    });
  };

  verifyTransaction = (filePath) => {
    return fetch(`${url}/verify-transaction/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        filePath
      })
    })
    .then((response) => {
      return response.json()
        .then(data => response.ok ? data : Promise.reject(data));
    });
  };

  miniPrintFile = async (file) => {
    const json = await fetch(`${url}/parse-xml?file=${file}`)
      .then(response => response.json());

    const parsed = JSON.parse(json);

    if (parsed.comprobanteType === "factura") {
      FacturaTab.miniPrint(parsed);
    }
  }

  miniPrintProforma = async (file) => {
    const json = await fetch(`${url}/file?file=${file}`)
      .then(response => response.json());

    //const parsed = JSON.parse(json);
    console.log('*********** miniPrintProforma, json:', json);

    ProformaTab.miniPrint(json);
  }

  editFile = async (file) => {
    const json = await fetch(`${url}/parse-xml?file=${file}`)
      .then(response => response.json())
      .then(json => { console.log('********* received json:', json); return json; });

    const parsed = JSON.parse(json);
    console.log('********** parsed:', parsed);

    if (parsed.comprobanteType === "factura") {
      this.setState({ tabIndex: 0 }, () => {
        this.facturaTabRef.edit(parsed);
      });
    } else if (parsed.comprobanteType === "guiaRemision") {
      this.setState({ tabIndex: 1 }, () => {
        this.guiaRemisionTabRef.edit(parsed);
      });
    }
  }

  editProforma = async (file) => {
    console.log('***** in App.js, editProforma, file:', file);

    const json = await fetch(`${url}/file?file=${file}`)
      .then(response => response.json())
      .then(json => { console.log('********* editProforma, received json:', json); return json; });

    this.setState({ tabIndex: 2 }, () => {
      this.proformaTabRef.edit(json);
    });
  }

  facturarFile = async (file) => {
    console.log('***** in App.js, facturarFile, file:', file);

    const json = await fetch(`${url}/parse-xml?file=${file}`)
      .then(response => response.json())
      .then(json => { console.log('********* received json:', json); return json; });

    const parsed = JSON.parse(json);
    console.log('********** parsed:', parsed);

    if (parsed.comprobanteType === "guiaRemision") {
      this.setState({ tabIndex: 0 }, () => {
        this.facturaTabRef.facturar(parsed);
      });
    }
  }

  facturarProforma = async (file) => {
    console.log('***** in App.js, facturarProforma, file:', file);

    const json = await fetch(`${url}/file?file=${file}`)
      .then(response => response.json())
      .then(json => { console.log('********* facturarProforma, received json:', json); return json; });

    //const parsed = JSON.parse(json);
    //console.log('********** parsed:', parsed);

    this.setState({ tabIndex: 0 }, () => {
      this.facturaTabRef.facturar(json);
    });
  }

  render() {
    console.log('React version', React.version);
    console.log('url', url);

    return (
      <AppBar
        labels={[
          "Crear Factura",
          "Crear Guía de Remisión",
          "Crear Proforma",
          "Proformas",
          "Pendientes",
          "Autorizados",
        ]}
        tabIndex={this.state.tabIndex}
        onTabChange={tabIndex => this.setState(state => ({ tabIndex, drawerOpen: (tabIndex === 0 ? state.drawerOpen : false) }))}
        drawer = {(settings) => {
          return (
            <Drawer
              open = {this.state.drawerOpen}
              //checked = {this.state.checked}
              checked = {[]}
              //setChecked = {checked => this.setState({ checked })}
              setChecked = {_ => {}}
              onDrawerClosed = {() => this.setState({ drawerOpen: false })}
            />);
        }}
        onDrawerOpen = {() => this.setState({ drawerOpen: true })}
        open = {this.state.drawerOpen}
        render = {(settings) => {
          //const { checked } = settings;
          const { tabIndex } = this.state;
          if (tabIndex === 0) {
            return (
              <FacturaTab
                ref = {e => this.facturaTabRef = e}
              />
            );
          } else if (tabIndex === 1) {
            return (
              <GuiaRemisionTab
                ref = {e => this.guiaRemisionTabRef = e}
              />
            );
          } else if (tabIndex === 2) {
            return (
              <ProformaTab
                ref = {e => this.proformaTabRef = e}
              />
            );
          } else if (tabIndex === 3) {
            return (
              <div>
                <SimpleTable
                  url = {`${url}/list-proformas`}
                  title = 'Proformas'
                  onFacturar = {this.facturarProforma}
                  onMiniPrint = {this.miniPrintProforma}
                  onEdit = {this.editProforma}
                />;
              </div>
            );
          } else if (tabIndex === 4) {
            return (
              <div>
                <TheTable
                  url = {`${url}/list-pending-authorization`}
                  title = 'Comprobantes Pendientes'
                  transactionState = 'pending'
                  onMiniPrint = {this.miniPrintFile}
                  onEdit = {this.editFile}
                />;
              </div>
            );
          } else if (tabIndex === 5) {
            return (
              <div>
                <TheTable
                  url = {`${url}/list-authorized`}
                  title = 'Comprobantes Autorizados'
                  transactionState = 'verified'
                  onFacturar = {this.facturarFile}
                  onMiniPrint = {this.miniPrintFile}
                  onEdit = {this.editFile}
                />;
              </div>
            );
          }
        }}
      />
    );
  }
}

export default App;
