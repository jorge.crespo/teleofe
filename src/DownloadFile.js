import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import FileDownloadIcon from '@material-ui/icons/CloudDownload';
import { useFilePicker } from 'use-file-picker';
import React from 'react';

const fileTypes = {
  multixlsx: {
    description: 'Excel 2007+ (XLSX)',
    accept: { 'multipart/form-data': ['.xlsx'] }
  },
  xlsx: {
    description: 'Excel 2007+ (XLSX)',
    accept: { 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': ['.xlsx'] }
  }
};

function useSaveFilePicker(props) {
  const [fileHandle, setFileHandle] = React.useState(null);
  const type = fileTypes[props.accept];
  const saveFileOptions: FilePickerOptions = {
    suggestedName: 'lista_de_precios_codificada.xlsx',
    types: [type]
  }

  //const openSaveFileSelector = async (content: string) => {
  const openSaveFileSelector = async function (content: string) {
    const fh = await window.showSaveFilePicker(saveFileOptions);
    setFileHandle(fh);
  };

  const clear = () => {
    setFileHandle(null);
  }

  return [openSaveFileSelector, { fileHandle, clear }];
}

export default function App(props) {
  const [openSaveFileSelector, { fileHandle, clear }] = useSaveFilePicker({
    accept: 'multixlsx',
  });

  React.useMemo(() => {
    if (fileHandle != null) {
      console.log('fileHandle changed, downloading file:', fileHandle);
      props.onFileChosen(fileHandle);
      clear();
    }
  }, [fileHandle]);

  return (
    <div>
      <ListItem button key={'productos'} onClick={openSaveFileSelector}>
        <ListItemIcon><FileDownloadIcon /></ListItemIcon>
        <ListItemText primary={'Bajar Productos'} />
      </ListItem>
    </div>
  );
}
