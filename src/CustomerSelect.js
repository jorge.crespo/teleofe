//import React, { Component } from 'react';
import React from 'react';
var Select = require('react-select');

export default class CustomerSelect extends React.Component
{
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      options: null,
    };
  }

  focus = () => this.ref.focus();

  onMenuOpen = () => this.setState({data: null});

  onChange = (selectedOption) => {
    if (this.state.data) {
      this.setState(state => ({...state, options: state.data}));
    }
    this.props.onCustomerSelected(selectedOption);
  }

  onCreateOption = (input) => {
    // Returns true if input is valid and change is done
    if (this.props.onCustomerCreated(input)) {
      if (this.state.options) {
        this.setState({data: null, options: null});
      }
    }
  }

  formatCreateLabel = (input) => `Crear "${input}"`;

  loadOptions = (input) => {
    return new Promise((resolve, reject) => {
      if (input && input.length > 0) {
        let ms = (input.length == 1)
          ? 1500
          : (input.length == 2) ? 1000 : 400;

        if (this.timeoutId) {
          clearTimeout(this.timeoutId);
        }
        this.timeoutId = setTimeout(() => {
          this.timeoutId = null;
          this.requestCustomers(input)
            .then(arts => {
              this.setState({data: arts});
              return resolve(arts);
             })
            .catch(e => reject(e));
        }, ms);
      } else {
        reject(null);
      }
    });
  };

  requestCustomers(input) {
    var url = this.props.queryUrl;
    if (input)
      url = url + input;
    //console.log('url name', url)
    return fetch(url)
      .then(response => response.json())
      .then(data => {
        return data && data.content
          ? data.content.map(this.props.mapCustomers)
          : Promise.reject(null);
      });
  }

  getStyles = (propStyles) => ({
    control: styles => {
      if (propStyles && propStyles.control) {
        styles = propStyles.control(styles);
      }
      return ({ ...styles });
    },

    option: (styles, { data, isDisabled, isFocused, isSelected, label }) => {
      let isNew = label.startsWith('Crear "');

      if (propStyles && propStyles.option) {
        styles = propStyles.option(styles);
      }

      return {
        ...styles,
        boxSizing: undefined,
        display: undefined,
        fontSize: 'inherit',
        backgroundColor: isDisabled
          ? null
          : isSelected
            ? styles.backgroundColor
            : isFocused
              ? isNew ? 'orange' : 'yellow'
              : styles.backgroundColor,
        fontWeight: isNew
          ? 'bold'
          : isSelected
            ? 500 : 'normal',
        cursor: isDisabled ? 'not-allowed' : 'default',
      };
    },
    singleValue: (base) => {

      if (propStyles && propStyles.singleValue) {
        base = propStyles.singleValue(base);
      }

      // fontSize inherited from valueContainer
      base = {...base, fontSize: 'inherit', padding: '2px 0px'};

      return this.props.isNewCustomer
        ? { ...base, fontWeight: 'bold', color: 'orange' }
        : base;
    },
    valueContainer: (base) => {

      if (propStyles && propStyles.valueContainer) {
        base = propStyles.valueContainer(base);
      }

      base = {...base, fontSize: '24px', padding: '2px 0px'};
      //base = {...base, fontSize: '1.5em'};

      return this.props.isNewCustomer
        //? { ...base, background: 'orange', width: '100%' }
        ? { ...base, width: '100%' }
        : base;
    },
    input: (base) => {

      if (propStyles && propStyles.input) {
        base = propStyles.input(base);
      }

      return this.props.isNewCustomer
        ? { ...base, color: 'black' }
        : base;
    },
  });

  cleanDefaultOptions = () => {
    this.setState({data: null, options: null});
  }

  setRef = (e) => {
console.log('in CustomerSelect: the e I got:', e);
    this.ref = e;
  }

  render() {
    return (
      <Select.AsyncCreatable
        ref={this.setRef}
        classes={this.props.classes}
        styles={this.getStyles(this.props.styles)}
        textFieldProps={this.props.textFieldProps}
        textFieldInputProps={this.props.textFieldInputProps}
        defaultOptions={this.state.options}
        loadOptions={this.loadOptions}
        components={this.props.components}
        value={this.props.selectedCustomerOption}
        onChange={this.onChange}
        onCreateOption={this.onCreateOption}
        formatCreateLabel={this.formatCreateLabel}
        onFocus={this.props.onFocus}
        onBlur={this.props.onBlur}
        placeholder={this.props.placeholder}
        onMenuOpen={this.onMenuOpen}
        isDisabled={this.props.isDisabled}
      />);
  }
}
