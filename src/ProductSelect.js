import React, { Fragment } from 'react';
import ProgressMobileStepper from './ProgressMobileStepper.js';

var Select = require('react-select');
var { components } = Select;

//const SingleValue = ({ children, ...props }) => {
//const SingleValue = (elparam) => {
const SingleValue = (props) => {
//console.log('elparam:', elparam);
  //let { children, ...props } = elparam;
  let data = props.data;

  let label = !data.unit || data.unit.toLowerCase() === 'c/u'
    ? data.articulo
    : data.unit + ' ' + data.articulo;

  return (
    <components.SingleValue {...props}>
      {label}
    </components.SingleValue>
  );
};

export default class ProductSelect extends React.Component {

  timeoutId = null;

  constructor(props) {
    super(props)

    this.state = {
      data: null,
      options: null,
      page: 1,
    };
  }

  onMenuOpen = () => this.setState({data: null});

  handleChange = (selectedOption) => {
    console.log('Option selected:', selectedOption);
    if (this.state.data) {
      this.setState(state => ({...state, options: state.data}));
    }
    this.props.onProductSelected(selectedOption);
  }

  getStyles = (propStyles) => ({

    //control: propStyles.control,
    control: styles => {
      if (propStyles && propStyles.control) {
        styles = propStyles.control(styles);
      }
      return styles;
    },

    option: (styles, { data, isDisabled, isFocused, isSelected, value }) => {
      let isElim = value === this.props.ELIM;

      if (propStyles && propStyles.option) {
        styles = propStyles.option(styles);
      }

      return {
        ...styles,
        padding: '0px',
        backgroundColor: isDisabled
          ? null
          : isSelected
            ? styles.backgroundColor
            : isFocused
              ? isElim ? 'red' : 'yellow'
              : styles.backgroundColor,
        fontWeight: isElim ? 'bold' : styles.fontweight,
      };
    },
    singleValue: (base) => {

      if (propStyles && propStyles.singleValue) {
        base = propStyles.singleValue(base);
      }

      // fontSize inherited from valueContainer
      return {...base, fontSize: 'inherit', padding: '5px 0px'};
    },
    valueContainer: (base) => {

      if (propStyles && propStyles.valueContainer) {
        base = propStyles.valueContainer(base);
      }

      //base = {...base, fontSize: '1.5em'};
      return {...base, fontSize: '24px', padding: '0px 0px'};
    },
    input: (base) => {

      if (propStyles && propStyles.input) {
        base = propStyles.input(base);
      }
      //return {...base, fontSize: '24px'};
      return base;
    },
  });

/*
        <div style={{ padding: '8px 12px' }}>
          Custom Menu with {optionsLength} options
        </div>
*/
  Menu = (props) => {
    const optionsLength = 75;
    return (
      <Fragment>
        <ProgressMobileStepper />
        <components.Menu {...props}>
          {props.children}
        </components.Menu>
      </Fragment>
    );
  };

/*
        <div style={menuHeaderStyle}>
          Custom Menu List
        </div>
*/
  MenuList = (props) => {
    return (
      <components.MenuList {...props}>
        <ProgressMobileStepper
          handleNext = {this.handleNext}
          handleBack = {this.handleBack}
        />
        {props.children}
      </components.MenuList>
    );
  };

  handleNext = () => {
    const arr = [{
        value: 'pipi',
        label: 'Popocito',
        codProveedor: '',
        articulo: 'Popocito',
        marcaPais: '',
        unit: '',
        price: '',
        sinIva: true,
    }];
    this.setState(state => ({ ...state, page: state.page + 1, data: arr, options: arr }), () => { console.log('POPOOO') });
  }

  handleBack = () => {
    this.setState(state => ({ ...state, page: state.page - 1 }));
  }

  loadOptions = (input, callback) => {
    return new Promise((resolve, reject) => {
      if (input && input.length > 0) {
        let ms = (input.length == 1)
          ? 1500
          : (input.length == 2) ? 1000 : 400;

        if (this.timeoutId) {
          clearTimeout(this.timeoutId);
        }
        this.timeoutId = setTimeout(() => {
          this.timeoutId = null;
          this.requestProducts(input)
            .then(arts => {
              this.setState({data: arts});
              return resolve(arts);
            })
            .catch(e => reject(e));
        }, ms);
      } else {
        reject(null);
      }
    });
  }

  requestProducts(input) {
    var url = 'http://localhost:3001/search-product?any='
    if (input)
      url = url + input
    console.log('url any', url)
    return fetch(url)
      .then(response => response.json())
      .then(data =>
        data && data.content
          ? this.mapProductsForSelect(data)
          : Promise.reject(null)
      );
  }

  mapProductsForSelect = (data) => {
    return data.content
      .map((item, index) => ({
        value: item.CODIGO,  // Server must ensure uniqueness of CODIGO
        label: item.ARTICULO,
        codProveedor: item.COD_PROVEEDOR,
        articulo: item.ARTICULO,
        marcaPais: item['MARCA PAIS'],
        unit: item.UNID,
        price: item['P.V.P.'],
        sinIva: item.SIN_IVA
      }))
      .concat([{
        value: this.props.ELIM,
        label: 'ELIMINAR...',
        codProveedor: '',
        articulo: 'ELIMINAR...',
        marcaPais: '',
        unit: '',
        price: '',
        sinIva: true
      }]);
  }

  formatOptionLabel = (option, FormatOptionLabelMeta) => {
    const genStyle = {border: '1px solid black'};
    return ( 
      <table style={{borderCollapse: 'collapse', width: '100%'}}>
        <tbody>
          <tr style={genStyle}>
            <td style={{...genStyle, width: '15%'}}>{option.codProveedor}</td>
            <td style={{...genStyle, padding: '10px 4px 10px 4px'}}>{option.articulo}</td>
            <td style={{...genStyle, width: '15%'}}>{option.marcaPais}</td>
            <td style={{...genStyle, width: '10%'}}>{option.unit}</td>
            <td style={{...genStyle, width: '10%', textAlign: 'right'}}>{option.price}</td>
          </tr>
        </tbody>
      </table>
    );
  }

  render() {
console.log('En ProductSelect render');
    //let genStyle = {border: '1px solid black'};

        //components={{ SingleValue }}
        //components={this.props.components}
    //this.props.components.SingleValue = SingleValue;
    let { Option, ...otherComponents } = this.props.components;
    otherComponents.SingleValue = SingleValue;
    //otherComponents.Menu = this.Menu;
    otherComponents.MenuList = this.MenuList;
        //key={JSON.stringify(this.state.page)}
    return (
      <Select.Async style={{display: 'inline'}}
        classes={this.props.classes}
        styles={this.getStyles(this.props.styles)}
        textFieldProps={this.props.textFieldProps}
        textFieldInputProps={this.props.textFieldInputProps}
        components={otherComponents}
        value={this.props.selectedOption}
        onChange={this.handleChange}
        defaultOptions={this.state.options}
        loadOptions={this.loadOptions}
        formatOptionLabel={this.formatOptionLabel}
        placeholder={this.props.placeholder}
        onMenuOpen={this.onMenuOpen}
        isDisabled={this.props.isDisabled}
      />
    );
  }
}
