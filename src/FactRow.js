import React from 'react';
import ProductSelect from './SyncProductSelect.js';
import ValueLabelColumn from './ValueLabelColumn.js';
import SimpleTextField from './SimpleTextField.js';
import IntegrationReactSelect from './IntegrationReactSelect.js';
/*
import {
  calculateTotal,
} from './helpers.js';
*/

import {
  rowCalcs,
} from './FactCalculations.js';


export default class FactRow extends React.Component
{
  static QUANTITY_ID = 101;
  static PRICE_ID = 103;
  static TOTAL_ID = 104;
  static NUM_DECIMALS = 2;

  truncDecimals(value) {
    const dotIndex = (value || '').indexOf(".");
    if (dotIndex != -1) {
      const decimals = value.substr(dotIndex + 1);
      value = value.substring(0, dotIndex + 1) + decimals.substr(0, this.constructor.NUM_DECIMALS);
    }
    return value;
  }

  onQuantityChanged = (value) => {
    value = this.truncDecimals(value);
    const total = rowCalcs.calculateTotal(value, this.props.price);
    this.props.onQuantityChanged(value, total);
  }

  onProductSelected = (selectedOption) => {
    const total = rowCalcs.calculateTotal(this.props.quantity, selectedOption.price);
    this.props.onProductChanged(selectedOption.value, selectedOption.articulo, selectedOption.unit, selectedOption.sinIva, selectedOption.price, total);
  }

  setInput(input) {
    if (this.productSelect) {
      this.productSelect.setInput(input);
    }
  }

  render() {
    return (
      <tr className={'Product-row'}>
        <td style={{width: '3em', boxSizing: 'border-box'}}>
          <SimpleTextField
            type = 'number'
            inputRef = {this.props.inputRefCallback}
            value = {this.props.quantity}
            onChange = {evt => this.onQuantityChanged(evt.target.value)}
            InputStyle = {{margin: '0px'}}
            inputProps = {{min: '0'}}
            disabled = {this.props.disabled}
          />
        </td>
        <td colSpan={2} style={{width: '950px'}}>
          <IntegrationReactSelect
            value = {this.props.selectedOption}
            render = {(innerProps) => (
              <ProductSelect
                {...innerProps}
                ref = {e => this.productSelect = e}
                onProductSelected = {this.onProductSelected}
                ELIM = {this.props.ELIM}
                isDisabled = {this.props.disabled}
              />)}
          />
        </td>
        <ValueLabelColumn id = {this.constructor.PRICE_ID} content={this.props.price}/>
        <ValueLabelColumn id = {this.constructor.TOTAL_ID} content={this.props.total}/>
      </tr>
    );
                //selectedOption = {innerProps.value}
  }
}
