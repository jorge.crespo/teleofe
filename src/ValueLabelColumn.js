import React from 'react';
import ComposedTextField from './ComposedTextField.js';
import InputAdornment from '@material-ui/core/InputAdornment';

export default class ValueLabelColumn extends React.Component
{
  endAdornment = () => <InputAdornment position="start">%</InputAdornment>;

  render() {
    const { style = {}, readOnly = true } = this.props;

    return (
      <td className = {'Product-row-label'} colSpan = {this.props.colSpan || 1} style = {{...style, border: '0px solid white'}}>
        {
          readOnly && <label id = {this.props.id} onDoubleClick = {this.props.onDoubleClick}>
            {this.props.content}
          </label>
        }
        {
          !readOnly && <ComposedTextField
            onLabelDoubleClick = {this.props.onLabelDoubleClick}
            label = {this.props.content}
            value = {this.props.inputValue}
            onChange = {this.props.onInputValueChanged}
            readOnly = {false}
            inputProps = {{
              endAdornment: this.endAdornment()
            }}
            disabled = {this.props.inputDisabled}
          />
        }
      </td>
    );
  }
}
