import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
//import DialogContentText from '@material-ui/core/DialogContentText';
import IconButton from '@material-ui/core/IconButton';
import EmailIcon from '@material-ui/icons/Email';
import PrintIcon from '@material-ui/icons/Print';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import printJS from 'print-js';

import { Document, Page } from 'react-pdf';
import XMLViewer from 'react-xml-viewer'

import EmailDialog from './EmailDialog';

const useStyles = makeStyles({
  sheet: {
    width: 'auto',
  },
  pdfCanvas: {
    '& .react-pdf__Page__canvas': {
      height: [['100%'], '!important'],
      width: [['100%'], '!important'],
    },
  },
  //paper: { minWidth: "1024px" },
  paper: { minWidth: "1040px" },
});

const { REACT_APP_API_URL: url } = process.env;

function handleMiniPrint(props) {
  props.setOpen(false);
  props.onMiniPrint(props.file);
}

function handleEdit(props) {
  props.setOpen(false);
  props.onEdit(props.file);
}

function handleMoveToSent(props) {
  props.setOpen(false);
  props.onMoveToSent(props.file);
}

function handleFacturable(props) {
  props.setOpen(false);
  props.onFacturar(props.file);
}

async function handlePrint(props) {
  //props.setOpen(false);

  //window.open(`${url}/file?file=${props.file}`);
/*
  const doc = new jsPDF();
  doc.loadFile(`${url}/file?file=${props.file}`, false, (param) => {
    //console.log('********** jsPDF.loadFile, param:', param);
    const thepdf = doc.output('save', 'toditapo.pdf');
    console.log('********** thepdf:', thepdf);
    //thepdf.autoPrint();
    //doc.output('dataurlnewwindow');
    //doc.output('pdfobjectnewwindow');
  });
*/
  //window.print(`${url}/file?file=${props.file}`);
  //window.print();

/*
  const blob = await fetch(`${url}/file?file=${props.file}`)
    .then(res => res.blob());
  const blobURL = URL.createObjectURL(blob);
console.log('***** blobURL:', blobURL);
  printJS(blobURL);
*/
console.log('***** props.file:', props.file);
  printJS(`${url}/file?file=${props.file}`);
}

function getTransactionState(file) {
  const splitted = file.split('.');
  return splitted.slice(-2, -1)[0];
}

function getErrorFileName(file) {
  const state = getTransactionState(file);

  const splitted = file.split('.');

  if (state == "signed") {
    return `${splitted.slice(0, 1)[0]}.send.error.xml`;
  }
  if (state == "sent") {
    return `${splitted.slice(0, 1)[0]}.verify.error.xml`;
  }
}

export default function TemporaryDialog(props) {
  const classes = useStyles();
  const [numPages, setNumPages] = React.useState(null);
  const [pageNumber, setPageNumber] = React.useState(1);
  const [xmlError, setXmlError] = React.useState('');
  const [isClaveAccesoAlreadyRegistered, setIsClaveAccesoAlreadyRegistered] = React.useState(false);
  const [emailDialogOpen, setEmailDialogOpen] = React.useState(false);
  const [email, setEmail] = React.useState('');

  function handleEmailable(props) {
    setEmailDialogOpen(true);
  }

  function onEmailDialogAccept(event) {
    setEmailDialogOpen(false);

    return fetch(`${url}/email-verified/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        filePath: props.file,
        email
      })
    })
    .then(response => response.json()
      .then(data => {
        setEmail('');
        return response.ok ? data : Promise.reject(data);
      })
    );
  }

  function onEmailDialogCancel(event) {
    setEmail('');
    setEmailDialogOpen(false);
  }

  function handleClose(props, event) {
    setXmlError('');
    props.onClose();
  }

  function onEmailChange(evt) {
    const email = evt.target ? evt.target.value : '';
    //email = email.replace(/[ ]/g, '');
    setEmail(email);
  }

              //className={classes.pdfCanvas} // put as a Page prop
        //options={{workerSrc: "pdf.worker.js"}} // https://stackoverflow.com/questions/73333451/getting-error-when-displaying-a-pdf-file-using-react-pdf
  const pdfSheet = side => {
    return (
    <div
      className={classes.sheet}
      role="presentation"
    >
      <Document
        file={`${url}/file?file=${props.file}`}
        onLoadSuccess={(pdf) => {
          setNumPages(pdf.numPages);
          setPageNumber(1);
          props.setLoaded(true);
        }}
      >
        {props.loaded && Array.from(
          new Array(numPages),
          (el, index) => (
            <Page
              key={`page_${index + 1}`}
              pageNumber={index + 1}
              width={1000}
            />
          ),
        )}
      </Document>
    </div>
    );
  };

  function isMiniPrintable() {
    return props.comprobanteType === 'fac';
  }

  function isEditable() {
    return props.hasError && props.transactionState === 'signed' && !isClaveAccesoAlreadyRegistered;
  }

  function isFacturable() {
    return props.comprobanteType === 'guiar' && props.transactionState === 'verified';
  }

  function isMovableFromSignedToSent() {
    return isClaveAccesoAlreadyRegistered && props.hasError && props.transactionState === 'signed';
  }

  function isEmailable() {
    return (props.comprobanteType === 'fac' || props.comprobanteType === 'ncr') && props.transactionState === 'verified';
  }

  const analyzeXmlError = (text) => {
    setIsClaveAccesoAlreadyRegistered(text.includes('<mensaje>CLAVE ACCESO REGISTRADA</mensaje>'));
    return text;
  }

  const errorSheet = () => {
console.log('********** in errorSheet, about to request file:', getErrorFileName(props.file));
    if (!xmlError) {
console.log('********** in errorSheet, requesting file:', getErrorFileName(props.file));
      fetch(`${url}/file?file=${getErrorFileName(props.file)}`)
        .then(response => response.text())
        .then(text => analyzeXmlError(text))
        .then(text => setXmlError(text))
        .catch(err => console.log('Error when retrieving xml file:', props.file, 'error:', err));
    }

    const customTheme = {
      //"attributeKeyColor": "#FF0000",
      //"attributeValueColor": "#000FF",
      "overflowBreak": true,
    };

    return (
    <div
      className={classes.sheet}
      role="presentation"
    >
      <XMLViewer xml={xmlError} theme={customTheme}/>
    </div>
    );
  }
        //fullWidth
        //maxWidth="md"
  return (
    <div>
      <Dialog
        open={props.open}
        onClose={event => handleClose(props, event)}
        scroll="body"
        classes={{ paper: classes.paper }}
      >
        <DialogActions>
          {isMiniPrintable() && <IconButton onClick={() => handleMiniPrint(props)}>
            <PrintIcon />
          </IconButton>}
          {isEmailable() && <IconButton onClick={() => handleEmailable(props)}>
            <EmailIcon />
          </IconButton>}
          {isFacturable() && <Button onClick={() => handleFacturable(props)}>
            Facturar
          </Button>}
          {isMovableFromSignedToSent() && <Button onClick={() => handleMoveToSent(props)}>
            Mover a enviado
          </Button>}
          {isEditable() && <Button onClick={() => handleEdit(props)}>
            Editar
          </Button>}
          <Button autoFocus onClick={() => handlePrint(props)}>
            Imprimir
          </Button>
          <Button onClick={event => handleClose(props, event)}>
            <CloseIcon />
          </Button>
        </DialogActions>

        <DialogContent>
          {props.hasError && errorSheet()}
          {pdfSheet('right')}
        </DialogContent>

        <DialogActions>
          <Button autoFocus onClick={() => handlePrint(props)}>
            Imprimir
          </Button>
          <Button onClick={event => handleClose(props, event)}>
            Cerrar
          </Button>
        </DialogActions>

      </Dialog>
      <EmailDialog
        open={emailDialogOpen}
        handleAccept={onEmailDialogAccept}
        handleCancel={onEmailDialogCancel}
        value={email}
        onValueChange={onEmailChange}
      />
    </div>
  );
}
