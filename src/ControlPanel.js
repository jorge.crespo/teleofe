import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';

import DatePicker from './DatePicker.js';
import CustomerSelect from './SyncCustomerSelect.js';
//import './App.css';

import ComposedTextField from './ComposedTextField.js';
import CircularPrint from './CircularPrint';
import SimpleTextField from './SimpleTextField.js';
import ValueLabelColumn from './ValueLabelColumn.js';
import FormaPagoColumn from './FormaPagoColumn.js';
import FactRow from './FactRow.js';

import IntegrationReactSelect from './IntegrationReactSelect.js';
import CustomizedSnackbar from './CustomizedSnackbar';

import {
  cedulaError,
  rucNaturalError,
  rucUnnaturalError
} from './CedRucVal.js';

const { REACT_APP_API_URL: url } = process.env;

class ControlPanel extends React.Component
{
  constructor(props) {
    super(props);

    this.nextInputElement = null;
    this.nameSelect = null;
    this.rucedSelect = null;
    this.transportistaNameSelect = null;
    this.transportistaRucedSelect = null;
    this.rowRefs = [];
    this.state = {
      snackbarOpen: false,
      snackbarMessage: '',
      dateOn: false,
      sequentialOn: false,
      discountOn: false
    };
  }

  discountRate = () => {
    return this.props.discountRate;
  }

  onSequentialChanged = evt => {
    let fact = evt.target ? evt.target.value : '';
    if (fact === '' || /^\d+$/.test(fact)) {
      this.props.onSequentialChanged(fact);
    }
  }

  changeDiscountRate = evt => {
    const dr = evt.target ? evt.target.value : '';
    if (dr === '' || /^(\d*)\.?(\d*)$/.test(dr)) {
      this.props.onDiscountRateChanged(dr);
    }
  }

  enableDiscountRate = () => {
    if (!this.props.isTransactionDisabled) {
      this.setState({ discountOn: true });
      this.props.onDiscountRateEnabled();
    }
  }

  disableDiscountRate = () => {
    if (!this.props.isTransactionDisabled) {
      this.setState({ discountOn: false });
      this.props.onDiscountRateDisabled();
    }
  }

  enableDatePicker = event => {
    if (!this.props.isTransactionDisabled) {
      if (event.detail === 2) this.setState({ dateOn: true });
    }
  }

  enableSequential = () => {
    if (!this.props.isTransactionDisabled) {
      this.setState({ sequentialOn: true });
    }
  }

  disableSequential = () => {
    if (!this.props.isTransactionDisabled) {
      this.setState({ sequentialOn: false });
      this.props.onResetSequential();
    }
  }

  onCustomerNameSelected = ({ name, ruced, address, phone, email }) => {
    this.props.onCustomerSelected({ name, ruced, address, phone, email });

    if (this.rucedSelect) {
      this.rucedSelect.cleanDefaultOptions();
    }
  }

/*
  onTransportistaNameSelected = ({ name, ruced, placa, destinationAddress }) => {
    this.props.onTransportistaSelected({ name, ruced, placa, destinationAddress });

    if (this.transportistaRucedSelect) {
      this.transportistaRucedSelect.cleanDefaultOptions();
    }
  }
 */

  onCustomerNameCreated = (input) => {
    this.props.onCustomerNameCreated(input);

    if (this.rucedSelect) {
      this.rucedSelect.cleanDefaultOptions();
    }

    return true;
  }

/*
  onTransportistaNameCreated = (input) => {
    this.props.onTransportistaNameCreated(input);

    if (this.transportistaRucedSelect) {
      this.transportistaRucedSelect.cleanDefaultOptions();
    }

    return true;
  }
 */

  onCustomerRucedSelected = ({ name, ruced, address, phone, email }) => {
    this.props.onCustomerSelected({ name, ruced, address, phone, email });

    if (this.nameSelect) {
      this.nameSelect.cleanDefaultOptions();
    }
  }

/*
  onTransportistaRucedSelected = ({ name, ruced, placa, destinationAddress }) => {
    this.props.onTransportistaSelected({ name, ruced, placa, destinationAddress });

    if (this.transportistaNameSelect) {
      this.transportistaNameSelect.cleanDefaultOptions();
    }
  }
 */

  validateRuced = (input) => {
    if (input.length === 10) {
      const snackbarMessage = cedulaError(input);
      if (snackbarMessage != null) {
        this.setState({ snackbarOpen: true, snackbarMessage });
        return false;
      }
    } else if (input.length === 13 && Number(input[2]) < 6) {
      const snackbarMessage = rucNaturalError(input);
      if (snackbarMessage != null) {
        this.setState({ snackbarOpen: true, snackbarMessage });
        return false;
      }
    } else if (input.length === 13 && (Number(input[2]) === 6 || Number(input[2]) === 9)) {
      const snackbarMessage = rucUnnaturalError(input);
      if (snackbarMessage != null) {
        this.setState({ snackbarOpen: true, snackbarMessage });
        return false;
      }
    } else {
      this.setState({ snackbarOpen: true, snackbarMessage: 'Número de cédula o RUC incorrecto' });
      return false;
    }
    return true;
  }

  onCustomerRucedCreated = (input) => {
    if (!this.validateRuced(input)) {
      return false;
    }

    this.props.onCustomerRucedCreated(input);

    if (this.nameSelect) {
      this.nameSelect.cleanDefaultOptions();
    }
    return true;
  }

/*
  onTransportistaRucedCreated = (input) => {
    if (!this.validateRuced(input)) {
      return false;
    }

    this.props.onTransportistaRucedCreated(input);

    if (this.transportistaNameSelect) {
      this.transportistaNameSelect.cleanDefaultOptions();
    }
    return true;
  }
 */

  onAddressChanged = (evt) => {
    let address = evt.target ? evt.target.value : '';
    this.props.onAddressChanged(address);
  }

  onPhoneChanged = (evt) => {
    const phone = evt.target ? evt.target.value : '';
    if (phone === '' || /^\d+$/.test(phone)) {
      this.props.onPhoneChanged(phone);
    } else {
      console.log('phone', phone, 'is not a number');
    }
  }

  onEmailChanged = (evt) => {
    let email = evt.target ? evt.target.value : '';
    email = email.replace(/[ ]/g, '');
    this.props.onEmailChanged(email);
  }

/*
  onPlacaChanged = (evt) => {
    const placa = evt.target ? evt.target.value : '';
    this.props.onPlacaChanged(placa);
  }
*/

  onDestinationAddressChanged = (evt) => {
    const address = evt.target ? evt.target.value : '';
    this.props.onDestinationAddressChanged(address);
  }

  getSelectedCustomerOption = (labelAttr, valueAttr) => {
    const option = this.props.customer;

    if (option === null) {
      return null;
    }

    return {
      ...option,
      label: option[labelAttr] || '',
      value: option[valueAttr] || ''
    };
  }

  getSelectedTransportistaOption = (labelAttr, valueAttr) => {
    const option = this.props.transportista;

    if (option === null) {
      return null;
    }

    return {
      ...option,
      label: option[labelAttr] || '',
      value: option[valueAttr] || ''
    };
  }

  selectedOption = (index) => {
    let row = this.props.rows[index];
    return {
        value: row.code,
        label: row.article,
        codProveedor: '',
        articulo: row.article,
        marcaPais: '',
        unit: row.unit,
        price: row.price,
        sinIva: row.noIva
    };
  }

  onLastTrFocus = (evt) => {
    this.nextRowToFocus = this.props.rows.length;
    this.props.pushEmptyRow();
  }

  componentDidUpdate = () => {
    if (this.nextInputElement) {
      this.nextInputElement.focus();
      this.nextInputElement = null;
      this.nextRowToFocus = null;
    }
    if (this.props.customer === null) {
      this.nameSelect.cleanDefaultOptions();
      this.rucedSelect.cleanDefaultOptions();
    }
    if (this.props.transportista === null) {
      this.transportistaNameSelect.cleanDefaultOptions();
      this.transportistaRucedSelect.cleanDefaultOptions();
    }
    if (this.justMounted) {
      this.nameSelect.focus();
      this.justMounted = false;
    }
  }

  componentDidMount = () => {
    //this.onDiscountRateDisabled();
    this.setState({ discountOn: false });
    this.setState({ sequentialOn: false });
    this.setState({ dateOn: false });
    this.justMounted = true;
  }

  inputRefCallback = (rowId, e) => {
    if (this.nextRowToFocus !== null) {
       this.nextInputElement = this.nextRowToFocus === rowId ? e : null;
    }
  }

  isAddressOn = () => true;
  isPhoneOn = () => true;
  isEmailOn = () => true;
  isDateOn = () => this.state.dateOn;
  isSequentialOn = () => this.state.sequentialOn;
  isDiscountOn = () => this.state.discountOn;

  setCustomer = (ruced) => {
    if (this.rucedSelect) {
      this.rucedSelect.setInput(ruced);
    }
  }

  setTransportista = (ruced) => {
    if (this.rucedSelect) {
      this.transportistaRucedSelect.setInput(ruced);
    }
  }

  setSelect = (attr, e) => {
    this[attr] = e;
  }

  setProduct(index, code) {
    if (this.rowRefs && this.rowRefs[index]) {
      this.rowRefs[index].setInput(code);
    }
  }

  render() {
    let rows = this.props.rows
      .map((row, index) =>
        <FactRow
          key = {row.id}
          ref = {e => this.rowRefs[index] = e}
          ELIM = {this.props.ELIM}
          inputRefCallback = {e => this.inputRefCallback(index, e)}
          quantity = {this.props.rows[index].quantity}
          price = {this.props.rows[index].price}
          total = {this.props.rows[index].total}
          onQuantityChanged = {(...args) => this.props.onQuantityChanged(index, ...args)}
          onProductChanged = {(...args) => this.props.onProductChanged(index, ...args)}
          selectedOption = {this.selectedOption(index)}
          disabled = {this.props.isTransactionDisabled}
        />);

    rows.push(
      <tr key = {1000} onFocus = {this.onLastTrFocus}>
        <td>
          <button>+</button>
        </td>
        <td colSpan={2} style={{width: '950px'}}>
        </td>
        <td/>
        <td/>
      </tr>);

    const summaryRowPadding = '0px';

/*
                style={{fontSize: 'inherit'}}
                classes={{
                  root: classes.cssLabel,
                  focused: classes.cssFocused,
                }}
 */
    return (
      <div className={'Control-panel'}>
        <table style={{width: '100%'}}><tbody>
          <tr>
            <td style={{width: '5%'}}>
              {/*<InputLabel
                htmlFor="custom-css-standard-input"
                style={{color: 'black'}}
              >
                {'Guayaquil,'}
              </InputLabel>*/}
              <label>
                {'Guayaquil,'}
              </label>
            </td>
            <td style={{width: '50%'/*, fontSize: '1rem'*/}}>
              {this.isDateOn()
                ? <DatePicker
                    value = {this.props.date}
                    onChange = {value => this.props.onDateChanged(value)}
                    isDisabled = {this.props.isTransactionDisabled}
                  />
                : <label onClick={this.enableDatePicker}>
                    {this.props.date.label}
                  </label>
              }
            </td>
            <td style={{width: '2%'}}>
              <label></label>
            </td>
            <td style={{width: '18%'}}>
              <ComposedTextField
                onInputDoubleClick={this.enableSequential}
                onLabelDoubleClick={this.disableSequential}
                label = {this.props.sequentialLabel}
                value = {this.props.sequential}
                onChange = {this.onSequentialChanged}
                readOnly = {!this.isSequentialOn()}
                disabled = {this.props.isTransactionDisabled}
              />
            </td>
            {this.props.guiaRemision != null && <td style={{width: '15%'}}>
              <ComposedTextField
                label = 'Guia de Remisión'
                kind = 'info'
                value = {this.props.guiaRemision}
                readOnly = {true}
                disabled = {this.props.isTransactionDisabled}
              />
            </td>}
            <td style={{width: this.props.guiaRemision != null ? '10%' : '25%', position: 'relative'}}>
              <CircularPrint
                disabled={this.props.printDisabled}
                onClick={this.props.onPrintClicked}
                onSuccessClicked={this.props.onSuccessClicked}
              />
            </td>
          </tr>
        </tbody></table>

        <table style={{width: '100%'}}><tbody>
          <tr>
            <td style={{width: '49%'}}>
              <IntegrationReactSelect
                label = {'Cliente'}
                value = {this.getSelectedCustomerOption('name', 'ruced')}
                render = {(innerProps) => (
                  <CustomerSelect
                    {...innerProps}
                    ref = {e => this.setSelect('nameSelect', e)}
                    selectedCustomerOption = {innerProps.value}
                    onCustomerCreated = {this.onCustomerNameCreated}
                    onCustomerSelected = {this.onCustomerNameSelected}
                    isNewCustomer = {this.props.isNewCustomer}
                    queryUrl = {`${url}/search-customer?name=`}
                    mapCustomers = {(item, index) => ({
                        ...item,
                        value: item.ruced,
                        label: item.name
                      })
                    }
                    isDisabled = {this.props.isCustomerDisabled}
                  />)}
              />
            </td>
            <td style={{width: '2%'}}>
              <label></label>
            </td>
            <td style={{width: '18%'}}>
              <IntegrationReactSelect
                label = {'RUC o Cédula'}
                value = {this.getSelectedCustomerOption('ruced', 'ruced')}
                render = {(innerProps) => (
                  <CustomerSelect
                    {...innerProps}
                    ref = {e => e ? (this.rucedSelect = e) : null}
                    selectedCustomerOption = {innerProps.value}
                    onCustomerCreated = {this.onCustomerRucedCreated}
                    onCustomerSelected = {this.onCustomerRucedSelected}
                    isNewCustomer = {this.props.isNewCustomer}
                    queryUrl = {`${url}/search-customer?ruced=`}
                    mapCustomers = {(item, index) => ({
                        ...item,
                        value: item.ruced,
                        label: item.ruced
                      })
                    }
                    isDisabled = {this.props.isCustomerDisabled}
                  />)}
              />
              <CustomizedSnackbar
                open = {this.state.snackbarOpen}
                handleClose = {(event, reason) => {
                  this.setState({ snackbarOpen: false });
                }}
                variant = 'error'
                message = {this.state.snackbarMessage}
              />
            </td>
            <td style={{width: '2%'}}>
              <label></label>
            </td>
            <td style={{width: '29%'}}>
              { this.isEmailOn()
                ? <SimpleTextField
                    label = {'Email'}
                    value = {this.props.customer.email || ''}
                    placeholder='Email'
                    type='email'
                    onChange = {this.onEmailChanged}
                    disabled = {this.props.isCustomerDisabled}
                  />
                : <label></label> }
            </td>
          </tr>
        </tbody></table>

        {
          (this.isAddressOn() || this.isPhoneOn())
            && <div style={{height: '5px'}} />
        }

        <table style={{width: '100%'}}><tbody>
          <tr>
            <td style={{width: '69%'}}>
              { this.isAddressOn()
                ? <SimpleTextField
                    label = {'Dirección'}
                    value = {this.props.customer.address || ''}
                    placeholder='Dirección'
                    onChange = {this.onAddressChanged}
                    disabled = {this.props.isCustomerDisabled}
                  />
                : <label></label> }
            </td>
            <td style={{width: '10%'}}>
              <label></label>
            </td>
            <td style={{width: '21%'}}>
              { this.isPhoneOn()
                ? <SimpleTextField
                    label = {'Teléfono'}
                    value = {this.props.customer.phone || ''}
                    placeholder='Teléfono'
                    onChange = {this.onPhoneChanged}
                    disabled = {this.props.isCustomerDisabled}
                  />
                : <label></label> }
            </td>
          </tr>
        </tbody></table>

        <div style={{height: '25px'}} />

        <table style={{width: '100%'}}><tbody>
          <tr className={'Product-header'}>
            <th style={{width: '8%'}}>CANT.</th>
            <th colSpan={2}>DESCRIPCION</th>
            <th style={{width: '10%'}}>V. UNIT.</th>
            <th style={{width: '10%'}}>V. TOTAL</th>
          </tr>
          {rows}
        </tbody></table>
        <div style={{ width: '100%', display: 'flex', flexWrap: 'wrap' }}>
        <table style={{ width: '50%' }}><tbody>
        {
          this.props.pagos.map(pago => (
            <tr key={`tr-pago-${pago.formaDePago}`} className={'Product-row'}>
              <FormaPagoColumn id = {220} colSpan={3}
                formasDePago={this.props.formasDePago}
                onFormaDePagoChanged={this.props.onFormaDePagoChanged}
                pago={pago}
                onValueChanged={this.props.onPagoValueChanged}
              />
            </tr>
          ))
        }
        </tbody></table>
        <table style={{ width: '50%' }}><tbody>
          <tr className={'Product-row'}>
            <td colSpan={2} style={{ padding: summaryRowPadding }}/>
            <ValueLabelColumn id = {211} colSpan={2} content='Merc. con IVA'/>
            <ValueLabelColumn id = {212} content={this.props.totalWithIva}/>
          </tr>
          <tr className={'Product-row'}>
            <td colSpan={3} style={{ padding: summaryRowPadding }}/>
            <ValueLabelColumn id = {221}
              onDoubleClick={this.enableDiscountRate}
              onLabelDoubleClick={this.disableDiscountRate}
              content='Descuento'
              inputValue={this.discountRate()}
              onInputValueChanged={this.changeDiscountRate}
              inputDisabled = {this.props.isTransactionDisabled}
              readOnly = {!this.isDiscountOn()}
            />
            <ValueLabelColumn id = {222} content={this.props.descuento}/>
          </tr>
          <tr className={'Product-row'}>
            <td colSpan={3} style={{ padding: summaryRowPadding }}/>
            <ValueLabelColumn id = {231} content='Sub Total'/>
            <ValueLabelColumn id = {232} content={this.props.subtotal}/>
          </tr>
          <tr className={'Product-row'}>
            <td colSpan={3} style={{ padding: summaryRowPadding }}/>
            <ValueLabelColumn id = {241} content='I.V.A. 0%'/>
            <ValueLabelColumn id = {242} content={this.props.zeroIva}/>
          </tr>
          <tr className={'Product-row'}>
            <td colSpan={3} style={{ padding: summaryRowPadding }}/>
            <ValueLabelColumn id = {251} content='I.V.A. 12%'/>
            <ValueLabelColumn id = {252} content={this.props.iva}/>
          </tr>
          <tr className={'Product-row'}>
            <td colSpan={3} style={{ padding: summaryRowPadding }}/>
            <ValueLabelColumn id = {261} content='TOTAL'/>
            <ValueLabelColumn id = {262} content={this.props.bigTotal}/>
          </tr>
        </tbody></table>
        </div>
      </div>
    );
  }
}

export default ControlPanel;
