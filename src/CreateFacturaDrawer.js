import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Switch from '@material-ui/core/Switch';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';

import DateIcon from '@material-ui/icons/CalendarToday';
import FactNumberIcon from '@material-ui/icons/Receipt';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import FilePicker from './FilePicker';
import FormData from 'form-data';
//import FileTypeChooser from './FileTypeChooser';
import DownloadFile from './DownloadFile';
import SimpleBackdrop from './SimpleBackdrop';
import InformationDialog from './InformationDialog';
import SimpleDialog from './SimpleDialog';

const { REACT_APP_API_URL: url } = process.env;

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  listRoot: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  formControl: {
    fontSize: 'inherit',
  },
  radioListItem: {
    paddingTop: '0px',
  },
}));

function sendFile(file) {
  const form = new FormData();
  form.append('file', file, file.name);
  return fetch(`${url}/upload-products/`, {
    method: 'POST',
    body: form,
  })
  .then(response => response.json()
    .then(data => {
      if (response.ok) {
        console.log('archivo uploaded exitosamente, data:', data);
        return data;
      } else {
        const err = new Error(`Error when uploading file, status: "${response.status}: ${response.statusText}"`);
        console.error(`Error when uploading file, status: "${response.status}: ${response.statusText}", data:`, data);
        err.response = response;
        err.data = data;
        throw err;
      }
    })
  )
  .catch(err => {
    console.log('falla al enviar el archivo', file, 'error:', err);
    throw err;
  });
}

const codedProductsWbName = 'CodedPL.xlsx';

async function receiveFile(fileHandle) {
  const url = `${process.env.REACT_APP_API_URL}/file?file=${codedProductsWbName}`;

  try {
    const writableStream = await fileHandle.createWritable();

    await fetch(url)
      .then(response => response.body)
      .then(response => response.pipeTo(writableStream));

    //await writableStream.close(); // When the readable stream closes, the writable closes too
    const file = await fileHandle.getFile();
    console.log('Finished writing file:', file);
  } catch(err) {
    console.error('Error when downloading file, error:', err);
    throw err;
  }
}

function CreateFacturaDrawer(props) {
  const [backdropOpen, setBackdropOpen] = React.useState(false);
  const [uploadedInfoDialogOpen, setUploadedInfoDialogOpen] = React.useState(false);
  const [uploadedInfoDialogMessages, setUploadedInfoDialogMessages] = React.useState([]);
  const [errorMessage, setErrorMessage] = React.useState('');
  const [errorDialogOpen, setErrorDialogOpen] = React.useState(false);
  const classes = useStyles();
  const theme = useTheme();

  function handleDrawerClose() {
    props.onDrawerClosed();
  }

  async function onUploadFileChosen(file) {
    setBackdropOpen(true);
    try {
      await sendFile(file);
      setBackdropOpen(false);
      setUploadedInfoDialogMessages(['Productos cargados exitósamente']);
      setUploadedInfoDialogOpen(true);
    } catch(error) {
      setBackdropOpen(false);
      setErrorMessage(error.message);
      setErrorDialogOpen(true);
    }
  }

  function onBackdropClick(event, reason) {
    console.log('***** onBackdropClick, event:', event);
    console.log('***** onBackdropClick, reason:', reason);
  }

  function handleUploadedInfoDialogAccept() {
    setUploadedInfoDialogOpen(false);
  }

  function handleUploadedInfoDialogClose(_, reason) {
    if (reason !== 'backdropClick') {
      handleUploadedInfoDialogAccept();
    }
  }

  function onErrorDialogClose() {
    setErrorDialogOpen(false);
  }

  async function onDownloadFileChosen(fileHandle) {
    setBackdropOpen(true);
    try {
      await receiveFile(fileHandle);
      setBackdropOpen(false);
      setUploadedInfoDialogMessages(['Productos descargados exitósamente']);
      setUploadedInfoDialogOpen(true);
    } catch(error) {
      setBackdropOpen(false);
      setErrorMessage(error.message);
      setErrorDialogOpen(true);
    }
  }

  return (
    <div>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={props.open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>

        <Divider />

        <List
          subheader={<ListSubheader>Productos</ListSubheader>}
          className={classes.listRoot}
        >
          <FilePicker forChosenFile={onUploadFileChosen} />
          <DownloadFile onFileChosen={onDownloadFileChosen}/>
        </List>

        <Divider />

      </Drawer>

      <SimpleBackdrop open={backdropOpen} onClick={onBackdropClick} />

      <InformationDialog
        title={'Resultado'}
        messageArray={uploadedInfoDialogMessages}
        open={uploadedInfoDialogOpen}
        onAccept={handleUploadedInfoDialogAccept}
        onClose={handleUploadedInfoDialogClose}
      />

      <SimpleDialog
        disableEscapeKeyDown
        message={errorMessage}
        open={errorDialogOpen}
        options={[]}
        onClose={onErrorDialogClose}
      />
    </div>
  );
}

export default CreateFacturaDrawer;
