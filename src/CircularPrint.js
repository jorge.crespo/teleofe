import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';
import Fab from '@material-ui/core/Fab';
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';
import PrintIcon from '@material-ui/icons/Print';
import btnColor from '@material-ui/core/colors/yellow';

const styles = theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    position: 'absolute',
    right: '20px',
    top: '0px',
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonNormal: {
    color: theme.palette.getContrastText(btnColor[500]),
    backgroundColor: btnColor[500],
    '&:hover': {
      backgroundColor: btnColor[700],
    },
  },
  buttonSuccess: {
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
  buttonError: {
    backgroundColor: red[500],
    '&:hover': {
      backgroundColor: red[700],
    },
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    top: -6,
    left: -6,
    zIndex: 1,
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
});

class CircularIntegration extends React.Component {
  state = {
    success: false,
    error: false,
    loading: false,
  };

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  handleButtonClick = () => {
    if (!this.state.loading) {
      if (this.state.success) {
        this.setState({
          loading: false,
          success: false,
          error: false,
        });
        this.props.onSuccessClicked();
      } else {
        this.setState({
          loading: true,
        }, () => {
          this.props.onClick()
          .then(result => {
            this.setState({
              loading: false,
              success: true,
              error: false,
            });
          })
          .catch(err => {
            this.setState({
              loading: false,
              success: false,
              error: true,
            });
          });
        });
      }
    }
  };

  render() {
    const { loading, success, error } = this.state;
    const { classes } = this.props;

    const buttonClassname = classNames(
      classes.buttonNormal,
      {
        [classes.buttonSuccess]: success,
        [classes.buttonError]: error,
      });

/*
        <div className={classes.wrapper}>
          <Button
            variant="contained"
            color="primary"
            className={buttonClassname}
            disabled={loading}
            onClick={this.handleButtonClick}
          >
            Accept terms
          </Button>
          {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
        </div>
*/
    return (
      <div className={classes.root}>
        <div className={classes.wrapper}>
          <Fab color="primary" className={buttonClassname} disabled={this.props.disabled} onClick={this.handleButtonClick}>
            {success ? <CheckIcon /> : (error ? <ErrorIcon /> : <PrintIcon />)}
          </Fab>
          {loading && <CircularProgress size={68} className={classes.fabProgress} />}
        </div>
      </div>
    );
  }
}

CircularIntegration.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CircularIntegration);
