export const rowCalcs = {

  calculateTotal: function (quantity, price) {
    const q = Number(quantity);
    if (isNaN(q)) {
      console.log('quantity is NaN!');
      return;
    }
    let p = Number(price);
    if (isNaN(p)) {
      console.log('price is NaN!');
      return;
    }
    return (q*p).toFixed(2);
  },

  calculateTaxBase: function (noIva, total, ivaFactor, discountRate) {
    return noIva ? total : (
      discountRate !== null
        ? Number(total) * (1 - Number(discountRate)/100)
        : Number(total) / (1 + ivaFactor)
      ).toFixed(2);
  },

  calculateDiscount: function (noIva, total, taxBase) {
    return noIva ? '0.00' : (Number(total) - Number(taxBase)).toFixed(2);
  }
};
