import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    borderColor: 'green',
  },
  input: {
    color: 'red',
    //fontSize: 'inherit',
  },
  infoInput: {
    color: theme.palette.primary.main,
    fontSize: 'inherit',
  },
  underline: {
    //'&:before': {
      //borderBottom: `1px solid white`
      //borderBottom: `1px solid ${theme.palette.background.paper}`
      //borderBottom: `1px solid ${theme.palette.background.paper}`
    //},
    '&:after': {
      //borderBottom: `2px solid ${theme.palette.secondary.main}`
      borderBottom: `2px solid red`
    },
    '&:hover:not($disabled):not($focused):not($error):before': {
      borderBottom: `2px solid green !important`,
    },
  },
  inputLabelRoot: {
    '&$focused': {
      color: 'red',
    },
  },
  focused: {},
  disabled: {},
  error: {},
});

class ComposedTextField extends React.Component {

  componentDidMount() {
    this.forceUpdate();
  }

  render() {
    const { classes, kind } = this.props;

    const inputLabelClasses = { root: classes.inputLabelRoot, focused: classes.focused };
    if (kind === 'info') {
      delete inputLabelClasses.root;
    }

    return (
      <div className={classes.container}>
        <FormControl className={classes.formControl} style = {{width: '100%'}}>
          <InputLabel
            classes={inputLabelClasses}
            htmlFor="component-simple"
            //style={{fontSize: 'inherit'}}
            onDoubleClick={this.props.onLabelDoubleClick}
          >
              {this.props.label}
          </InputLabel>
          <Input
            id="component-simple"
            onDoubleClick={this.props.onInputDoubleClick}
            readOnly={this.props.readOnly}
            disableUnderline={this.props.readOnly}
            classes={{underline: classes.underline}}
            className={kind == 'info' ? classes.infoInput : classes.input}
            value={this.props.value || ''}
            onChange={this.props.onChange}
            disabled={this.props.disabled}
            {...this.props.inputProps}
          />
        </FormControl>
      </div>
    );
  }
}

ComposedTextField.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ComposedTextField);
