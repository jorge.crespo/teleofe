function range(start, end) {
  return Array.from(Array(end-start).keys(), x => x + start);
}

export { range }
