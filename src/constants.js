export const formasDePago = {
  EFECTIVO: { code: '01', label: 'Efectivo' },  // SIN_UTILIZACION_DEL_SISTEMA_FINANCIERO
  COMPENSACION_DE_DEUDAS: { code: '15' },
  TARJETA_DE_DEBITO: { code: '16', label: 'Tarjeta de débito' },  // TARJETAS_DE_DEBITO
  DINERO_ELECTRONICO: { code: '17' },
  TARJETA_PREPAGO: { code: '18' },
  TARJETA_DE_CREDITO: { code: '19', label: 'Tarjeta de crédito' },
  OTROS_CON_UTILIZACION_DEL_SISTEMA_FINANCIERO: { code: '20' },
  ENDOSO_DE_TITULOS: { code: '21' },
  CREDITO: { code: '999', label: 'Crédito' }
};
