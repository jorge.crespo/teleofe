import React from 'react';
//import ReactDOM from 'react-dom';
import ReactDOMServer from 'react-dom/server';

var PrintTemplate = require ('react-print');

/*
     date,
      total_elements: 0,
      rows: Array.from(new Array(N), (val,index) => (
        { quantity: '1',
          article: null,
          noIva: null,
          price: null,
          total: null
        })),
      totalWithIva: '0.00',
      totalWithoutIva: '0.00',
      zeroIva: '0.00',
      iva: '0.00',
      bigTotal: '0.00',
      selectedCustomerOption: null,
      isNewCustomer: false,
      address: '',
      phone: ''
*/

export default class MyTemplate extends React.Component
{
  render() {
    let payload = this.props.payload;
console.log('payload', payload);
    let rows = payload.rows
      .filter(row => row.article !== null)
      .map((row, index) => (
        <tr key = {index}>
          <td valign={'top'} align={'right'}>{`${row.quantity} `}</td>
          <td valign={'top'}>{row.article}</td>
          <td valign={'top'} align={'right'}>{row.price}</td>
          <td valign={'top'} align={'right'}>{row.total}</td>
        </tr>
      ));
console.log('rows', rows);
    let theNiceDate = payload.date.date.format('D [de] MMMM [del] YYYY');
    let reactToPrint = (
      <PrintTemplate>
        <div style={{fontSize: '10px'}}>
          <p>
            {`Guayaquil, ${theNiceDate}`}
          </p>
          <p style={{margin: '0'}}>
            {`Nombre: ${payload.selectedCustomerOption.name}`}
          </p>
          <p style={{margin: '0'}}>
            {`Cédula: ${payload.selectedCustomerOption.ruced}`}
          </p>
          <p style={{margin: '0'}}>Dirección: {payload.address}</p>
          <p style={{margin: '0'}}>Teléfono: {payload.phone}</p>
          <table><tbody>
           <tr>
             <th>CANT.</th><th>DESC.</th><th>PRECIO</th><th>TOTAL</th>
           </tr>
           {rows}
          </tbody></table>
          <p>The CSS will hide the original content and show what is in your
          Print Template.</p>
        </div>
      </PrintTemplate>
    );
    let htmlToPrint = ReactDOMServer.renderToStaticMarkup(reactToPrint);
    htmlToPrint = `<html><body>${htmlToPrint}</body></html>`
console.log('htmlToPrint', htmlToPrint);
    return reactToPrint;
  }
}

//ReactDOM.render(<MyTemplate/>, document.getElementById('root'));

