import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    //margin: theme.spacing.unit,
    margin: 'none',
  },
  cssLabel: {
    '&$cssFocused': {
      color: '#2684FF',
    },
  },
  cssFocused: {},
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: '#2684FF',
    },
  },
  notchedOutline: {},
});

function CustomizedInputs(props) {
  const { classes } = props;
  const inputProps = props.inputProps || {};

  return (
    <div className={classes.root}>
      <TextField
        className={classes.margin}
        InputLabelProps={{
          classes: {
            root: classes.cssLabel,
            focused: classes.cssFocused,
          },
        }}
        InputProps={{
          classes: {
            root: classes.cssOutlinedInput,
            focused: classes.cssFocused,
            notchedOutline: classes.notchedOutline,
          },
          inputProps: {
            ...inputProps,
            spellCheck: false,
            style: {fontSize: '1.5em', padding: '6px 5px 5px 5px'},
          },
        }}
        label={props.label}
        id="custom-css-outlined-input"
        value={props.value}
        placeholder={props.placeholder}
        style={{width: '100%'}}
        onChange={props.onChange}
        type={props.type || 'text'}
        inputRef={props.inputRef}
        variant="outlined"
      />
    </div>
  );
}

CustomizedInputs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CustomizedInputs);
