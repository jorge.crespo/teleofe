const { jsPDF } = require("jspdf");
import moment from 'moment';
require('moment/locale/es.js');

function createHeaders(keys) {
  return keys.map(({ id, name = id, prompt = id, width = 65, align = "center", padding = 0 }) => ({
    id,
    name,
    prompt,
    width,
    align,
    padding
  }));
}

const lmargin = 6;
const lborder = lmargin - 1;
const tmargin = 10;
const oneLineProductLength = 40;

export default function GenerateProformaPDF(obj) {
  //const doc = new jsPDF({ putOnlyUsedFonts: true, orientation: "landscape" });
  const doc = new jsPDF({ putOnlyUsedFonts: true });
  const { RUCED = '' } = obj;
  const isRuc = RUCED.length === 13;

  let currentTop = tmargin;
  const middle = lmargin + 134;
  const mlborder = middle - 1;
  const lineHeight = 6;

  const { issuer } = obj;

  doc.setTextColor('black');
  doc.setFontSize(14);

  doc.rect(mlborder, currentTop - 6, 64.5, 14);
  doc.text('PROFORMA', middle, currentTop);
  doc.setTextColor('red');
  doc.text(obj.sequential, middle + 34, currentTop);
  currentTop += lineHeight;

/*
  doc.rect(middle, currentTop - 6, 65, 19);
  doc.text('PROFORMA', middle, currentTop);
  currentTop += lineHeight;
  doc.text('No.', middle, currentTop);
  doc.setTextColor('red');
  doc.text(obj.sequential, middle + 9, currentTop);
  currentTop += lineHeight - 1;
*/

  doc.setTextColor('black');
  doc.setFontSize(10);
  const date = moment(
    `${obj.ANIO}-${obj.MES}-${obj.DIA}`,
    `${obj.yearFormat}-${obj.monthFormat}-${obj.dayFormat}`
  );
  doc.text(`Fecha: ${date.format('D')} de ${date.format('MMMM')} del ${date.format('YYYY')}`, middle, currentTop);
  //currentTop += lineHeight;

  currentTop = tmargin;

  doc.setTextColor('black');
  doc.setFontSize(14);
  doc.text('Ferrycons', lmargin, currentTop);
  currentTop += lineHeight;

  doc.setFontSize(10);

  doc.text(`RUC: ${issuer.ruc}`, lmargin, currentTop);
  currentTop += lineHeight;

  doc.text(`${issuer.razonSocial}`, lmargin, currentTop);
  currentTop += lineHeight;

  doc.text(`Dirección: ${issuer.direccion}`, lmargin, currentTop);
  currentTop += lineHeight;

  currentTop += lineHeight;

  doc.setTextColor('black');
  doc.setFontSize(10);

  doc.rect(lborder, currentTop - 5, 198.5, 14);
  doc.text(`Cliente: ${obj.CLIENTE}`, lmargin, currentTop);
  currentTop += lineHeight;

  doc.text(`${isRuc ? 'RUC' : 'Cédula'}: ${RUCED}`, lmargin, currentTop);
  currentTop += lineHeight;

  const numberWidth = 30;
  const headers = createHeaders([
    {
      id: "quantity",
      prompt: "Cant.",
      width: 20,
      align: "right"
    },
    {
      id: "article",
      prompt: "Artículo",
      width: 94.5,
      align: "left"
    },
    {
      id: "unit",
      prompt: "Unidad",
      width: 30
    },
    {
      id: "price",
      prompt: "Precio",
      width: numberWidth,
      align: "right"
    },
    {
      id: "taxBase",
      //prompt: "Base im-\nponible",
      prompt: "Base\nimponible",
      width: numberWidth,
      align: "right"
    },
    {
      id: "discount",
      prompt: "Descuento",
      width: numberWidth,
      align: "right"
    },
    {
      id: "total",
      prompt: "Total",
      width: numberWidth,
      align: "right"
    }
  ]);

  const rowCount = obj.rows.reduce((acum, row) =>
    acum + 1 + Math.trunc(row.article.length / oneLineProductLength) * 0.4,
    0.0
  );

  doc.table(lborder, currentTop, obj.rows, headers, { autoSize: false, fontSize: 10 });

  //const { pages } = doc.internal;
  //const numPages = pages.length - 1;
  const numPages = doc.internal.getNumberOfPages();
  console.log('**** number of pages:', numPages);

  const tableLineHeight = lineHeight + 4;

  currentTop +=
    rowCount * tableLineHeight +      // Height of total rows added
    2 * tableLineHeight * numPages +  // Height of total headers added
    2 * numPages;                     // Extra space

  //console.log('************ the doc.internal is:', doc.internal);
  //console.log('************ the doc.internal.getCurrentPageInfo() is:', doc.internal.getCurrentPageInfo());

  const rmargin = lborder + 196.5;
  const xsummary = rmargin - 20;

  const pageHeight = Math.trunc(doc.internal.pageSize.height);
  if (currentTop >= pageHeight) {
    currentTop -= (numPages - 1) * pageHeight;
    if (currentTop >= pageHeight) {
      console.log('currentTop still greater than pageHeight, it should never enter here!, currentTop:', currentTop);
      currentTop -= pageHeight;
    }
    if (currentTop <= tmargin) {
      doc.addPage();
      currentTop = tmargin;
    }
  }
  doc.text('Merc. con IVA:', xsummary, currentTop, { align: 'right' });
  doc.text(obj.TOTAL_CON_IVA, rmargin, currentTop, { align: 'right' });
  currentTop += lineHeight;

  if (currentTop >= pageHeight) {
    currentTop = tmargin;
    doc.addPage();
  }
  doc.text('Descuento:', xsummary, currentTop, { align: 'right' });
  doc.text(obj.DESCUENTO, rmargin, currentTop, { align: 'right' });
  currentTop += lineHeight;

  if (currentTop >= pageHeight) {
    currentTop = tmargin;
    doc.addPage();
  }
  doc.text('Sub Total:', xsummary, currentTop, { align: 'right' });
  doc.text(obj.SUBTOTAL, rmargin, currentTop, { align: 'right' });
  currentTop += lineHeight;

  if (currentTop >= pageHeight) {
    currentTop = tmargin;
    doc.addPage();
  }
  doc.text('IVA 0%:', xsummary, currentTop, { align: 'right' });
  doc.text(obj.IVA_ZERO, rmargin, currentTop, { align: 'right' });
  currentTop += lineHeight;

  if (currentTop >= pageHeight) {
    currentTop = tmargin;
    doc.addPage();
  }
  doc.text('IVA 12%:', xsummary, currentTop, { align: 'right' });
  doc.text(obj.IVA, rmargin, currentTop, { align: 'right' });
  currentTop += lineHeight;

  if (currentTop >= pageHeight) {
    currentTop = tmargin;
    doc.addPage();
  }
  doc.text('TOTAL:', xsummary, currentTop, { align: 'right' });
  doc.text(obj.TOTAL, rmargin, currentTop, { align: 'right' });
  currentTop += lineHeight;

  return { doc, uri: doc.output('datauristring') };
}
    
/*
var generateData = function(amount) {
  var result = [];
  var data = {
    coin: "100",
    game_group: "GameGroup",
    game_name: "XPTO2",
    game_version: "25",
    machine: "20485861",
    vlt: "0"
  };
  for (var i = 0; i < amount; i += 1) {
    data.id = (i + 1).toString();
    result.push(Object.assign({}, data));
  }
  return result;
};

function createHeaders(keys) {
  var result = [];
  for (var i = 0; i < keys.length; i += 1) {
    result.push({
      id: keys[i],
      name: keys[i],
      prompt: keys[i],
      width: 65,
      align: "center",
      padding: 0
    });
  }
  return result;
}
 */

/*
var headers = createHeaders([
  "id",
  "coin",
  "game_group",
  "game_name",
  "game_version",
  "machine",
  "vlt"
]);
 */


