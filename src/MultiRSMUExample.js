import * as React from "react";
import { MultipleSelect } from "react-select-material-ui";
 
class MultiRSMUExample extends React.Component {
  render() {
    const options: string[] = ["New York", "London", "Vienna", "Budapest"];
 
    return (
      <div className="App">
        <MultipleSelect
          label="Choose some cities"
          placeholder='Guayabita'
          values={["London", "Vienna"]}
          options={options}
          helperText="You can add a new city by writing its name and pressing enter"
          onChange={this.handleChange}
          SelectProps={{
            isCreatable: true,
            msgNoOptionsAvailable: "All cities are selected",
            msgNoOptionsMatchFilter: "No city name matches the filter"
          }}
        />
      </div>
    );
  }
 
  handleChange = (values: string[]) => {
    console.log(values);
  };
}
 
export default MultiRSMUExample;
