import React from 'react';
import makeSelectAsync from './MyAsyncSelect.js';

import { Creatable, components } from 'react-select';

const Comp = makeSelectAsync(Creatable);


export default class CustomerSelect extends React.Component
{
  focus = () => this.ref.focus();

  formatCreateLabel = (input) => `Crear "${input}"`;

  onEditChange(e) {
    //const { createdOption } = this.state;

    e.stopPropagation();
/*
    var newOption = Object.assign(createdOption, {
      label: e.target.value,
      value: e.target.value
    });

    this.setState({
      createdOption: newOption
    });
*/
  }

  SingleValue = props => {
console.log('** props:', props);
    return (<components.SingleValue {...props}>
      <input
        type="text"
        value={props.data.value}
        onChange={this.onEditChange.bind(this)}
        onKeyDown={e => {
          if (e.target.value.length > 0) {
            // important: this will allow the use of backspace in the input
            e.stopPropagation();
          } else if (e.key === "Enter") {
            // bonus: if value is empty, press enter to delete custom option
            e.stopPropagation();
            props.setValue(null);
          }
        }}
      />
    </components.SingleValue>);
  };

  styles = ({
    option: (styles, { data, isDisabled, isFocused, isSelected, label }) => {
      const isNew = label.startsWith('Crear "');
      return {
        ...styles,
        boxSizing: undefined,
        display: undefined,
        fontSize: 'inherit',
        backgroundColor: isDisabled
          ? null
          : isSelected
            ? styles.backgroundColor
            : isFocused
              ? isNew ? 'orange' : 'yellow'
              : styles.backgroundColor,
        fontWeight: isNew
          ? 'bold'
          : isSelected
            ? 500 : 'normal',
        cursor: isDisabled ? 'not-allowed' : 'default',
      };
    },

    singleValue: (base) => {
      // fontSize inherited from valueContainer
      base = {...base, fontSize: 'inherit', padding: '2px 0px'};

      return this.props.isNewCustomer
        ? { ...base, fontWeight: 'bold', color: 'orange' }
        : base;
    },

    valueContainer: (base) => {
      base = {...base, fontSize: '1.5em', padding: '2px 0px'};

      return this.props.isNewCustomer
        ? { ...base, width: '100%' }
        : base;
    },
  });

  requestCustomers = (input, pageNumber) => {
    var url = this.props.queryUrl;
    if (input)
      url = url + input;
    url = url + `&pageNumber=${pageNumber}`;
    return fetch(url)
      .then(response => response.json())
      .then(data => {
        return data && data.content
          ? {...data, content: data.content.map(this.props.mapCustomers)}
          : Promise.reject(null);
      });
  }

  cleanDefaultOptions = () => {
    if (this.ref) {
      this.ref.cleanDefaultOptions();
    }
  };

  setRef = (e) => {
    this.ref = e;
  }

  render() {
    return (
      <Comp
        {...this.props}
        components = {{ ...this.props.components, SingleValue: this.SingleValue }}
        ref={this.setRef}
        styles={{...this.props.styles, ...this.styles}}
        onOptionSelected={this.props.onCustomerSelected}
        onCreateOption={this.props.onCustomerCreated}
        formatCreateLabel={this.formatCreateLabel}
        request={this.requestCustomers}
      />
    );
  }
}
