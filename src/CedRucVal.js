function getCheckDigit(digits, coef) {
  const arr = [...coef].map(e => Number(e));
  const zipped = digits.slice(0, coef.length).map((d, index) => [d, coef[index]]);

  const reduced = zipped.reduce((total, a) => {
    let v = a[0]*a[1];
    if (v > 9) {
      v -= 9;
    }
    return total + v;
  }, 0);

  const mod = reduced % 10;
  return mod === 0 ? 0 : 10 - mod;
}

function cedulaError(input) {
  if (input.length !== 10) {
    return 'La cédula no tiene 10 caracteres';
  }

  if (!/^\d+$/.test(input)) {
    return 'Hay dígitos en la cédula que no son números';
  }

  const province = Number(input.substr(0, 2));
  if (province === 0 || province > 24) {
    return 'El código de provincia en la cédula es incorrecto';
  }
  
  if (Number(input[2]) > 5) {
    return 'El tercer dígito de la cédula no es menor a 6';
  }

  const digits = [...input].map(e => Number(e));
  const checkDigit = getCheckDigit(digits, '212121212')
  return checkDigit === digits[9] ? null : 'El dígito verificador de la cédula es incorrecto';
}

function rucNaturalError(input) {
  if (input.length !== 13) {
    return 'El RUC de persona natural no tiene 13 caracteres';
  }

  if (input.substr(-3) === '000') {
    return 'El RUC de persona natural no puede terminar en 000';
  }

  if (!/^\d+$/.test(input)) {
    return 'Hay dígitos en el RUC que no son números';
  }

  const province = Number(input.substr(0, 2));
  if (province === 0 || province > 24) {
    return 'El código de provincia en el RUC es incorrecto';
  }
  
  if (Number(input[2]) > 5) {
    return 'El tercer dígito del RUC no es menor a 6';
  }

  const digits = [...input].map(e => Number(e));
  const checkDigit = getCheckDigit(digits, '212121212')
  return checkDigit === digits[9] ? null : 'El dígito verificador del RUC es incorrecto';
}

function rucUnnaturalError(input) {
  if (input.length !== 13) {
    return 'El RUC no tiene 13 caracteres';
  }

  if (!/^\d+$/.test(input)) {
    return 'Hay dígitos en el RUC que no son números';
  }

  const province = Number(input.substr(0, 2));
  if (province === 0 || province > 24) {
    return 'El código de provincia en el RUC es incorrecto';
  }
  
  const juridicoOExtranjeroSinCedula = Number(input[2]) === 9; // Juridicos y extranjeros sin cedula
  const publico = Number(input[2]) === 6; // Publicos

  if (!juridicoOExtranjeroSinCedula && !publico) {
    return 'El RUC no es ni jurídico (o extranjero sin cédula) ni público';
  }

  if (juridicoOExtranjeroSinCedula && input.substr(-3) === '000') {
    return 'El RUC de un jurídico no puede terminar en 000';
  }

  if (publico && input.substr(-4) === '0000') {
    return 'El RUC público no puede terminar en 0000';
  }

  const coef = publico ? '32765432': '432765432';
  const arr = [...coef].map(e => Number(e));
  const digits = [...input].map(e => Number(e));
  const zipped = digits.slice(0, coef.length).map((d, index) => [d, coef[index]]);
  const reduced = zipped.reduce((total, a) => total + a[0]*a[1], 0);
  const mod = reduced % 11;
  const checkDigit = mod === 0 ? 0 : 11 - mod;

  const checkCorrect = publico ? digits[8] === checkDigit : digits[9] === checkDigit;

  return checkCorrect
    ? null
    : `El dígito verificador del RUC ${publico ? 'público' : 'jurídico (o extranjero sin cédula)'} es incorrecto`;
}

exports.cedulaError = input => null;
exports.rucNaturalError = input => null;
exports.rucUnnaturalError = input => null;
//exports.cedulaError = cedulaError;
//exports.rucNaturalError = rucNaturalError;
//exports.rucUnnaturalError = rucUnnaturalError;

//console.log('***** validation result:', cedulaError('0915379275'));
//console.log('***** validation result:', rucNaturalError('0900580002001'));
//console.log('***** validation result:', rucUnnaturalError('1790085783001'));
//console.log('***** validation result:', rucUnnaturalError('1760001040001'));
