import React, { Component } from 'react';
import moment from 'moment';
import chrono from 'chrono-node';

import Select from 'react-select';
import { components as SelectComponents } from 'react-select';

require('moment/locale/es.js');
moment.locale('es');

const createOptionForDate = d => {
  const date = moment.isMoment(d) ? d : moment(d);
  return {
    date,
    value: date.toDate(),
    label: date.calendar(null, {
      sameDay: '[hoy] (D [de] MMMM YYYY)',
      nextDay: '[mañana] (D [de] MMMM YYYY)',
      nextWeek: 'dddd [siguiente] (D [de] MMMM YYYY)',
      lastDay: '[ayer] (D [de] MMMM YYYY)',
      lastWeek: 'dddd [pasado] (D [de] MMMM YYYY)',
      sameElse: 'D [de] MMMM YYYY',
    }),
  };
};

export { createOptionForDate };

const defaultOptions = ['today', 'tomorrow', 'yesterday'].map(i =>
  createOptionForDate(chrono.parseDate(i))
);

const today = defaultOptions[0];
export { today };

const createCalendarOptions = (date = new Date()) => {
  // $FlowFixMe
  const daysInMonth = Array.apply(null, {
    length: moment(date).daysInMonth(),
  }).map((x, i) => {
    const d = moment(date).date(i + 1);
    return { ...createOptionForDate(d), display: 'calendar' };
  });
  return {
    label: moment(date).format('MMMM YYYY'),
    options: daysInMonth,
  };
};

defaultOptions.push(createCalendarOptions());

const suggestions = [
/*
  'sunday',
  'saturday',
  'friday',
  'thursday',
  'wednesday',
  'tuesday',
  'monday',
  'december',
  'november',
  'october',
  'september',
  'august',
  'july',
  'june',
  'may',
  'april',
  'march',
  'february',
  'january',
  'yesterday',
  'tomorrow',
  'today',
].reduce((acc, str) => {
  for (let i = 1; i < str.length; i++) {
    acc[str.substr(0, i)] = str;
  }
*/
  { label: 'domingo'   , value: 'domingo' },
  { label: 'sabado'    , value: 'sabado' },
  { label: 'sábado'    , value: 'sábado' },
  { label: 'viernes'   , value: 'viernes' },
  { label: 'jueves'    , value: 'jueves' },
  { label: 'miércoles' , value: 'miércoles' },
  { label: 'miercoles' , value: 'miercoles' },
  { label: 'martes'    , value: 'martes' },
  { label: 'lunes'     , value: 'lunes' },
  { label: 'diciembre' , value: 'december' },
  { label: 'noviembre' , value: 'november' },
  { label: 'octubre'   , value: 'october' },
  { label: 'septiembre', value: 'september' },
  { label: 'agosto'    , value: 'august' },
  { label: 'julio'     , value: 'july' },
  { label: 'junio'     , value: 'june' },
  { label: 'mayo'      , value: 'may' },
  { label: 'abril'     , value: 'april' },
  { label: 'marzo'     , value: 'march' },
  { label: 'febrero'   , value: 'february' },
  { label: 'enero'     , value: 'january' },
  { label: 'ayer'      , value: 'ayer' },
  { label: 'mañana'    , value: 'mañana' },
  { label: 'hoy'       , value: 'hoy' },
].reduce((acc, obj) => {
  let str = obj.label;
  for (let i = 1; i <= str.length; i++) {
    acc[str.substr(0, i)] = obj.value;
  }
  return acc;
}, {});

const suggest = str =>
  str
    //.split(/\b/)
    .split(/[ ]+/)
    .map(i => suggestions[i] || i)
    //.join('');
    .join(' ');

const days = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'];

const daysHeaderStyles = {
  marginTop: '5px',
  paddingTop: '5px',
  paddingLeft: '2%',
  borderTop: '1px solid #eee',
};
const daysHeaderItemStyles = {
  color: '#999',
  cursor: 'default',
  fontSize: '75%',
  fontWeight: '500',
  display: 'inline-block',
  width: '12%',
  margin: '0 1%',
  textAlign: 'center',
};
const daysContainerStyles = {
  paddingTop: '5px',
  paddingLeft: '2%',
};

const Group = props => {
  const { Heading, getStyles, children, label, innerProps, headingProps, cx, theme } = props;
  return (
    <div aria-label={label} style={getStyles('group', props)} {...innerProps}>
      <Heading
        theme={theme}
        getStyles={getStyles}
        cx={cx}
        {...headingProps}
      >
        {label}
      </Heading>
      <div style={daysHeaderStyles}>
        {days.map((day, i) => (
          <span key={`${i}-${day}`} style={daysHeaderItemStyles}>
            {day}
          </span>
        ))}
      </div>
      <div style={daysContainerStyles}>{children}</div>
    </div>
  );
};

const getOptionStyles = defaultStyles => ({
  ...defaultStyles,
  display: 'inline-block',
  width: '12%',
  margin: '0 1%',
  textAlign: 'center',
  borderRadius: '4px',
});

const Option = props => {
  const { data, getStyles, innerRef, innerProps } = props;
  if (data.display === 'calendar') {
    const defaultStyles = getStyles('option', props);
    const styles = getOptionStyles(defaultStyles);
    if (data.date.date() === 1) {
      const indentBy = data.date.day();
      if (indentBy) {
        styles.marginLeft = `${indentBy * 14 + 1}%`;
      }
    }
    return (
      <span {...innerProps} style={styles} ref={innerRef}>
        {data.date.format('D')}
      </span>
    );
  } else return <SelectComponents.Option {...props} />;
};

export default class DatePicker extends Component<*, *> {
  state = {
    options: defaultOptions,
  };
  handleInputChange = value => {
    if (!value) {
      this.setState({ options: defaultOptions });
      return;
    }
    const date = chrono.es.parseDate(suggest(value.toLowerCase()));
    if (date) {
      this.setState({
        options: [createOptionForDate(date), createCalendarOptions(date)],
      });
    } else {
      this.setState({
        options: [],
      });
    }
  };
  render() {
    const { value } = this.props;
    const { options } = this.state;
    return (
      <Select
        {...this.props}
        components={{ Group, Option }}
        filterOption={null}
        isMulti={false}
        isOptionSelected={(o, v) => v.some(i => i.date.isSame(o.date, 'day'))}
        maxMenuHeight={380}
        onChange={this.props.onChange}
        onInputChange={this.handleInputChange}
        options={options}
        value={value}
      />
    );
  }
}
