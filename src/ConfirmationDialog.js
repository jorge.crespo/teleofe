import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function ConfirmationDialog(props) {

  const { title, message, onYes, onNo, onClose, ...other } = props;

  function handleYes() {
    onYes();
  }

  function handleNo() {
    if (onNo)
      onNo();
  }

  function handleClose(event, reason) {
    if (onClose)
      onClose(event, reason);
  }

  return (
    <Dialog
      {...other}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {message}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleYes} color="primary">
          Si
        </Button>
        <Button onClick={handleNo} color="primary" autoFocus>
          No
        </Button>
      </DialogActions>
    </Dialog>
  );
}
