import * as React from 'react';
import { useTheme, withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import FilterListIcon from '@material-ui/icons/FilterList';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

//const ITEM_HEIGHT = 48;
//const ITEM_PADDING_TOP = 8;
const ITEM_HEIGHT = 28;
const ITEM_PADDING_TOP = 0;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const _comprobanteTypes = [
  { key: 'fac', name: 'Facturas' },
  { key: 'guiar', name: 'Guías de remisión' },
  { key: 'ncr', name: 'Notas de crédito' },
];

function getStyles(typeKey, comprobanteTypes, theme) {
  return {
    fontWeight:
      comprobanteTypes.find(t => t.key === typeKey) === undefined
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

export default function MultipleSelectChip(props) {
  const theme = useTheme();
  //const { classes } = props;
  const [comprobanteTypes, setComprobanteTypes] = React.useState([]);

  const handleChange = (event) => {
    event.stopPropagation();
    const {
      target: { value },
    } = event;
console.log('************ handleChange, value:', value);
    setComprobanteTypes(
      // On autofill we get a stringified value.
      //typeof value === 'string' ? value.split(',') : value,
      value
    );
    props.onChange(value);
  };

/*
          input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
          renderValue={(selected) => (
            <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
              {selected.map((value) => (
                <Chip key={value} label={value} />
              ))}
            </Box>
          )}
          MenuProps={MenuProps}
          classes={classes}
*/
  return (
    <div className="MultipleSelectChip">
      {/*<FormControl sx={{ m: 1, width: 300 }}>
        <InputLabel id="demo-multiple-chip-label">Chip</InputLabel>*/}
          {/*value={personName}*/}
        <Select
          disableUnderline
          labelId="demo-multiple-chip-label"
          id="demo-multiple-chip"
          multiple
          value={comprobanteTypes}
          onChange={handleChange}
          IconComponent={FilterListIcon}
          renderValue={selected => []}
        >
          {_comprobanteTypes.map(_t => (
            /*<MenuItem
              key={name}
              value={name}
              style={getStyles(name, personName, theme)}
            >
              {name}
            </MenuItem>*/
            <MenuItem key={_t.key} value={_t}>
              <Checkbox checked={comprobanteTypes.find(t => t.key === _t.key) !== undefined} />
              <ListItemText primary={_t.name} />
            </MenuItem>
          ))}
        </Select>
      {/*</FormControl>*/}
    </div>
  );
}

/*
const styles = theme => ({
  root: {
    "& $notchedOutline": {
      borderWidth: 0
    },
    "&:hover $notchedOutline": {
      borderWidth: 0
    },
    "&$focused $notchedOutline": {
      borderWidth: 0
    }
  },
  focused: {},
  notchedOutline: {}
});
*/

//export default withStyles(styles)(MultipleSelectChip);
