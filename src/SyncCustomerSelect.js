import React from 'react';
import makeSelectAsync from './MyAsyncSelect.js';

import { Creatable } from 'react-select';

const Comp = makeSelectAsync(Creatable);


export default class CustomerSelect extends React.Component
{
  focus = () => this.ref.focus();

  formatCreateLabel = (input) => `Crear "${input}"`;

  styles = ({
    option: (styles, { data, isDisabled, isFocused, isSelected, label }) => {
      const isNew = label.startsWith('Crear "');
      return {
        ...styles,
        boxSizing: undefined,
        display: undefined,
        fontSize: 'inherit',
        backgroundColor: isDisabled
          ? null
          : isSelected
            ? styles.backgroundColor
            : isFocused
              ? isNew ? 'orange' : 'yellow'
              : styles.backgroundColor,
        fontWeight: isNew
          ? 'bold'
          : isSelected
            ? 500 : 'normal',
        cursor: isDisabled ? 'not-allowed' : 'default',
      };
    },

    singleValue: (base) => {
      // fontSize inherited from valueContainer
      base = {...base, fontSize: 'inherit', padding: '2px 0px'};

      return this.props.isNewCustomer
        ? { ...base, fontWeight: 'bold', color: 'orange' }
        : base;
    },

    valueContainer: (base) => {
      base = {
        ...base,
        //fontSize: '1.5em',
        padding: '2px 0px'
      };

      return this.props.isNewCustomer
        ? { ...base, width: '100%' }
        : base;
    },
  });

  requestCustomers = (input, pageNumber) => {
    var url = this.props.queryUrl;
    if (input)
      url = url + input;
    url = url + `&pageNumber=${pageNumber}`;
    return fetch(url)
      .then(response => response.json())
      .then(data => {
        return data && data.content
          ? {...data, content: data.content.map(this.props.mapCustomers)}
          : Promise.reject(null);
      });
  }

  cleanDefaultOptions = () => {
    if (this.ref) {
      this.ref.cleanDefaultOptions();
    }
  };

  setRef = (e) => {
    this.ref = e;
  }

  setInput(input) {
console.log('******* SyncCustomerSelect, setInput, this.ref:', this.ref);
console.log('******* SyncCustomerSelect, setInput, input:', input);
    this.ref.onInputChange(input, { action: 'set-unique' }, ([option]) => {
console.log('******* SyncCustomerSelect callback, option:', option);
      this.props.onCustomerSelected(option);
      this.ref.onInputChange('', { action: 'set-value' });
    });
  }

  render() {
    return (
      <Comp
        {...this.props}
        ref={this.setRef}
        styles={{...this.props.styles, ...this.styles}}
        onOptionSelected={this.props.onCustomerSelected}
        onCreateOption={this.props.onCustomerCreated}
        formatCreateLabel={this.formatCreateLabel}
        request={this.requestCustomers}
      />
    );
  }
}
