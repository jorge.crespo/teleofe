import React, { Component } from 'react';
import ProgressMobileStepper from './ProgressMobileStepper.js';
import { components } from 'react-select';

export const defaultProps = {
  cacheOptions: false,
  defaultOptions: false,
  filterOption: null,
};

export default (SelectComponent) =>
class MyAsyncSelect extends Component {
  static defaultProps = defaultProps;
  timeoutId = null;

  lastRequest: {};
  mounted = false;

  constructor(props) {
    super(props)

    this.state = {
      inputValue: '',
      isLoading: false,
      loadedOptions: [],
      passEmptyOptions: false,
      pageNumber: null,
      pageCount: null,
      defaultInputValue: null,
      defaultOptions: null,
      defaultPageNumber: null,
      defaultPageCount: null,
    };
  }

  componentDidMount() {
    this.mounted = true;
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  focus() {
    this.select.focus();
  }

  blur() {
    this.select.blur();
  }

  handleChange = (selectedOption) => {
    this.props.onOptionSelected(selectedOption);
  }

/*
  Menu = (props) => {
    const optionsLength = 75;
    return (
      <Fragment>
        <ProgressMobileStepper />
        <components.Menu {...props}>
          {props.children}
        </components.Menu>
      </Fragment>
    );
  };
*/

  MenuList = (props) => {
    const { pageNumber, pageCount, defaultPageNumber, defaultPageCount } = this.state;
    const pc = pageCount == null ? defaultPageCount : pageCount;
    const pn = pageNumber == null ? defaultPageNumber : pageNumber;
    return (
      <components.MenuList {...props}>
        {pc != null && pc > 1 && <ProgressMobileStepper
          steps = {pc}
          activeStep = {pn - 1}
          handleNext = {this.handleNext}
          handleBack = {this.handleBack}
        />}
        {props.children}
      </components.MenuList>
    );
  };

  handleNextOrBack = (delta) => {
    const request = (this.lastRequest = {});
    this.setState({
      isLoading: true,
    },
    () => {
      const { inputValue, pageNumber, defaultInputValue, defaultPageNumber } = this.state;
      const iv = inputValue === '' ? defaultInputValue : inputValue;
      const pn = pageNumber == null ? defaultPageNumber : pageNumber;
      this.props.request(iv, pn + delta)
        .then(arts => {
          if (!this.mounted) return;
          if (request !== this.lastRequest) return;
          delete this.lastRequest;
          this.setState(state => ({
            ...state,
            isLoading: false,
            inputValue: iv,
            loadedInputValue: iv,
            loadedOptions: arts.content || [],
            passEmptyOptions: false,
            pageNumber: pn + delta,
            pageCount: arts.pageCount,
          }));
        });
    });
  }

  handleNext = () => this.handleNextOrBack(1);

  handleBack = () => this.handleNextOrBack(-1);

  onInputChange = (inputValue, action, callback) => {
    if (!inputValue) {
      delete this.lastRequest;
      this.setState(state => {
        let result = {
          ...state,
          inputValue: '',
          loadedInputValue: '',
          loadedOptions: [],
          isLoading: false,
          passEmptyOptions: false,
          pageNumber: null,
          pageCount: null,
        };
        if (action.action === 'set-value' && state.pageNumber != null) {
          result = {
            ...result,
            defaultInputValue: state.loadedInputValue,
            defaultOptions: state.loadedOptions,
            defaultPageNumber: state.pageNumber,
            defaultPageCount: state.pageCount,
          };
        }
        return result;
      });
      return;
    }

    const request = (this.lastRequest = {});
    this.setState({
      inputValue,
      isLoading: true,
      passEmptyOptions: !this.state.loadedInputValue,
    },
    () => {
      this.loadOptions(inputValue, action)
        .then(arts => {
          if (!this.mounted) return;
          if (request !== this.lastRequest) return;
          delete this.lastRequest;
          this.setState({
            isLoading: false,
            loadedInputValue: inputValue,
            loadedOptions: arts.content || [],
            passEmptyOptions: false,
            pageNumber: 1,
            pageCount: arts.pageCount,
          }, () => {
            if (callback) {
              callback(arts.content || []);
            }
          });
        });
    });
  }


  loadOptions = (input, action) => {
    return new Promise((resolve, reject) => {
      if (input && input.length > 0) {
        if (action && action.action === 'set-unique') {
          console.log('set-unique, input:', input);
          this.props.request(input, 1, action)
            .then(resolve)
            .catch(reject);
          return;
        }
        const ms = (input.length === 1)
          ? 800
          : (input.length === 2) ? 600 : 400;

        if (this.timeoutId) {
          clearTimeout(this.timeoutId);
        }
        this.timeoutId = setTimeout(() => {
          this.timeoutId = null;
          this.props.request(input, 1, action)
            .then(resolve)
            .catch(reject);
        }, ms);
      } else {
        reject(null);
      }
    });
  }

  cleanDefaultOptions = () => {
    this.setState({
      defaultInputValue: null,
      defaultOptions: null,
      defaultPageNumber: null,
      defaultPageCount: null,
    });
  }

  render() {
    const {
      inputValue,
      isLoading,
      loadedInputValue,
      loadedOptions,
      passEmptyOptions,
      defaultOptions,
    } = this.state;

    const options = passEmptyOptions
      ? []
      : inputValue && loadedInputValue
        ? loadedOptions
        : defaultOptions || [];

    return (
      <SelectComponent style={{display: 'inline'}}
        {...this.props}
        ref={ref => {
          this.select = ref;
        }}
        components={{...this.props.components, MenuList: this.MenuList}}
        onChange={this.handleChange}
        options={options}
        onInputChange={this.onInputChange}
        isLoading={isLoading}
        filterOption={null}
      />
    );
  }
}
