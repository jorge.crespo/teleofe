import * as React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function EmailDialog(props) {
  const handleClose = () => {
    console.log('*************** EmailDialog, handleClose');
  };

  return (
    <div>
      <Dialog open={props.open} onClose={handleClose}>
        <DialogTitle>Enviar por email</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Escriba el email al que desea enviar este comprobante.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Dirección de email"
            type="email"
            fullWidth
            variant="standard"
            value={props.value}
            onChange={props.onValueChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.handleCancel}>Cancelar</Button>
          <Button onClick={props.handleAccept}>Enviar</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
